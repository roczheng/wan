/// @Author rozheng
/// @Date 2024/4/30 19:01
/// @Description TODO
class R{
  static const String assetsImagesApplication= 'assets/images/3.0x/application_icon.png';
  static const String assetsImagesLoginBackground= 'assets/images/3.0x/bg_login.webp';
  static const String assetsIntegralRanking = 'assets/images/3.0x/ranking_integral.png';



  ///----------------------------lottie---------------------------
  static const String assetsLottieLoading = 'assets/lottie/lottie_common_load.json';
  static const String assetsLottiePageLoading= 'assets/lottie/page_loading_anim.json';
  static const String assetsLottieRefreshFooter = 'assets/lottie/refresh_footer.json';
  static const String assetsLottieRefreshHeader = 'assets/lottie/refresh_head_loading.json';
  static const String assetsLottieRefreshEmpty = 'assets/lottie/refresh_empty_page.json';
  static const String assetsLottieRefreshError = 'assets/lottie/refresh_error.json';
  static const String assetsLottieCollect = 'assets/lottie/collect_success.json';

  ///----------------------------SVG---------------------------
  static const String assetsImagesApplicationTransparent= 'assets/svg/application_transparent_icon.svg';
  static const String assetsImagesEyeVisible= 'assets/svg/eye_visible_icon.svg';
  static const String assetsImagesDefaultHeader= 'assets/svg/user_header_default_icon.svg';
  static const String assetsImagesSearch = 'assets/svg/search.svg';
  static const String assetsImagesMedalGold = 'assets/svg/medal_gold.svg';
  static const String assetsImagesMedalBronze = 'assets/svg/medal_bronze.svg';
  static const String assetsImagesMedalSilver = 'assets/svg/medal_silver.svg';
  static const String assetsImagesPoints= 'assets/svg/points.svg';
  static const String assetsImagesRubbish= 'assets/svg/rubbish.svg';
  static const String assetsImagesHotWord = 'assets/svg/hotword.svg';
  static const String assetsImagesHotWordItem = 'assets/svg/hotword_item.svg';
  static const String assetsImagesWechat = 'assets/svg/wechat.svg';
  static const String assetsImagesShare = 'assets/svg/share.svg';
  static const String assetsImagesCollect = 'assets/svg/collect.svg';
  static const String assetsImagesCollectQuit = 'assets/svg/collect_quit.svg';
  static const String assetsImagesProgram = 'assets/svg/program.svg';
  static const String assetsImagesDateTime= 'assets/svg/date_time.svg';
}