import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wan/res/string_styles.dart';
import 'package:wan/routes/routes.dart';
import 'package:wan/ui/page/demo_module/controller.dart';
import 'package:wan/ui/page/register_page/register_binding.dart';
import 'package:wan/ui/page/register_page/register_page.dart';
import 'package:wan/ui/page/splash_page/splash_binding.dart';
import 'package:wan/ui/page/splash_page/splash_page.dart';
import 'package:wan/util/Injection.dart';
import 'package:wan/util/KeyboardUtils.dart';
import 'package:wan/util/locale_util.dart';
import 'package:wan/util/save/sp_util.dart';

import 'widget/custom_theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Injection.init();
  runApp(GetMaterialApp(
    getPages: Routes.routePage,
    builder: (context, child) => Scaffold(
      ///监听手势返回
      // Global GestureDetector that will dismiss the keyboard
      body: GestureDetector(
        onTap: () {
          KeyboardUtils.hideKeyboard(context);
        },
        child: child,
      ),
    ),

    ///国际化支持-来源配置
    translations: Messages(),

    ///国际化支持-默认语言
    locale: LocaleOptions.getDefault(),

    ///国际化支持-备用语言
    fallbackLocale: const Locale('en', 'US'),

    ///路由跳转动画
    defaultTransition: Transition.fade,
    initialBinding: SplashBinding(),
    home: const SplashPage(),
    theme: lightTheme,
    darkTheme: darkTheme,
    themeMode: SpUtil.getAppTheme() == 0
        ? MediaQuery.of(Get.context!!).platformBrightness == Brightness.dark
            ? ThemeMode.dark
            : ThemeMode.light
        : SpUtil.getAppTheme() == 1
            ? ThemeMode.light
            : ThemeMode.dark,
  )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a purple toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: false, //false 可以去掉TabBar 底部默认的横线
        //useMaterial3: true,
        // 去除TabBar底部线条
        //tabBarTheme: const TabBarTheme(dividerColor: Colors.transparent),
      ),
      initialBinding: RegisterBinding(),
      home: const RegisterPage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final controller = Get.put(Controller());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("counter")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GetBuilder<Controller>(
                builder: (_) => Text(
                      'clicks: ${controller.count}',
                    )),
            ElevatedButton(
              child: Text('Next Route'),
              onPressed: () {
                Get.to(Second());
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            controller.increment();
          }),
    );
  }
}

class Second extends StatelessWidget {
  final Controller ctrl = Get.find();

  @override
  Widget build(context) {
    return Scaffold(body: Center(child: Text("${ctrl.count}")));
  }
}
