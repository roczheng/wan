import 'package:flutter/cupertino.dart';

/// @Author rozheng
/// @Date 2023/4/16 23:11
/// @Description TODO
class KeyboardUtils{
  ///隐藏软键盘
  ///[context] 上下文
  static hideKeyboard(BuildContext context) {
    FocusScopeNode currentFocus = FocusScope.of(context);
    if (!currentFocus.hasPrimaryFocus && currentFocus.focusedChild != null) {
      FocusManager.instance.primaryFocus?.unfocus();
    }
  }
}