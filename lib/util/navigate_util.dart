import 'package:url_launcher/url_launcher.dart';

/// @Author rozheng
/// @Date 2023/5/11 20:47
/// @Description TODO
class NavigateUtil{
  ///打开浏览器
  ///[url] 链接
  static Future<void> launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      throw 'Could not launch $url';
    }
  }

}