
import 'package:get/get.dart';
import 'package:wan/util/save/sp_util.dart';
import '../entity/project_entity.dart';
import '../entity/web_model.dart';
import '../routes/routes.dart';

/// @Author rozheng
/// @Date 2023/5/11 20:30
/// @Description TODO
class WebUtil {
  ///普通页面进入Web页面1
  static toWebPage(ProjectDetail detail , {Function(bool)? onResult}) {
    Get.toNamed(Routes.webViewPage, arguments: WebEntity(
      title: detail.title,
      link: detail.link,
      id: detail.id,
      isCollect: detail.collect,
    ))?.then((value) async{
      if(value is bool  && onResult != null){
        onResult(value);
      }
    });
    ///存储浏览记录
    SpUtil.saveBrowseHistory(detail);
  }
}