import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

/// @Author rozheng
/// @Date 2023/5/7 22:31
/// @Description 屏幕工具类
class ScreenUtil {
  ///去除状态栏半透明
  ///[context] 上下文
  static removeSystemTransparent(BuildContext context) {
    /// android 平台
    if (Theme.of(context).platform == TargetPlatform.android) {
      SystemUiOverlayStyle _style = const SystemUiOverlayStyle(
        statusBarColor: Colors.transparent,
        statusBarIconBrightness: Brightness.dark,
        statusBarBrightness: Brightness.light,
      );
      SystemChrome.setSystemUIOverlayStyle(_style);
    }
  }
}