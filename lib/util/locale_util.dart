import 'dart:ui';
import 'package:get/get.dart';
import 'package:wan/util/save/sp_util.dart';

import '../entity/language.dart';

/// @Author rozheng
/// @Date 2023/4/16 21:40
/// @Description TODO
class LocaleOptions{

  ///更新语言
  static updateLocale(Language language){
    Locale? locale;
    if(language.language == '' || language.country == ''){
      locale = Get.deviceLocale;
    }else{
      locale =  Locale(language.language , language.country);
    }
    if(locale != null){
      print("更新语言"+locale.languageCode);
      Get.updateLocale(locale);
    }
  }


  ///获取当前存储的默认语言
  static getDefault(){
    var language = SpUtil.getLanguage();
    if(language == null || language.language == '' || language.country == ''){
      return Get.deviceLocale;
    }else{
      return Locale(language.language , language.country);
    }
  }
}