import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

/// @Author rozheng
/// @Date 2023/4/16 21:25
/// @Description TODO
class ToastUtils {
  static show(String name) {
    Fluttertoast.showToast(msg: name,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: Colors.black,
        textColor: Colors.white,
        fontSize: 16.0);
  }
}