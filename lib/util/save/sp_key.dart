/// @Author rozheng
/// @Date 2023/4/16 21:46
/// @Description TODO
class SPKey{

  ///用户信息
  static const String keyUserInfo = 'user_info';
  ///国际化语言环境
  static const String language = 'language';
  ///搜索历史记录
  static const String searchHistory = 'searchHistory';
  ///浏览历史记录
  static const String browseHistory = 'browseHistory';
  ///主题
  static const String themeKey = 'themeKey';

}