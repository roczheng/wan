import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../http/request_repository.dart';
/// @Author rozheng
/// @Date 2023/4/16 22:58
/// @Description 初始化时进行依赖注入，可全局使用
class Injection{

  static Future<void> init() async {
    // shared_preferences
    await Get.putAsync(() => SharedPreferences.getInstance());
    Get.lazyPut(() =>RequestRepository());
  }
}