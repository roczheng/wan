import 'package:permission_handler/permission_handler.dart';


class PermissionRequest {


  static sendPermission(Function(bool) result) {
    Permission.camera.status.then((value) {
      if (value.isGranted) {
        result(true);
      } else {
        Permission.storage
            .request()
            .then((value){
          result(value.isGranted);
        });
      }
    });
  }
}