import 'package:flutter/material.dart';
import 'package:wan/res/color.dart';

import '../../../../entity/project_entity.dart';
import '../../../../res/decoration_style.dart';
import '../../../../res/style.dart';
import 'package:flutter_html/flutter_html.dart';

/// @Author rozheng
/// @Date 2024/5/15 10:33
/// @Description TODO
class MainArticleItem extends StatelessWidget {
  ///列表数据
  ProjectDetail item;

  int index = 0;

  MainArticleItem({super.key, required this.item, required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 16,top: 4,right: 16,bottom: 4),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: DecorationStyle.customize(ColorStyle.color_F8F9FC, 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Box.vBox10,
          Stack(
            children: [
              Text(
                getPaddingText(item.title),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: Styles.style_black_16_bold,
              ),
              Visibility(
                  visible: index <= 2,
                  child: Container(
                    margin: const EdgeInsets.only(top: 4),
                    padding: const EdgeInsets.symmetric(horizontal: 3 , vertical: 2),
                    decoration: DecorationStyle.customize(Colors.red, 3),
                    child: const Text(
                      '荐',
                      style: Styles.style_white_10,
                    ),
                  ))
            ],
          ),
          Visibility(
            visible: item.desc.isNotEmpty,
            child: Html(data: item.desc),
          ),
          Box.vBox10,
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                item.superChapterName,
                style: Styles.style_FE8C28_11,
              ),
              Box.hBox10,
              const Text(
                '|',
                style: Styles.style_9F9EA6_11,
              ),
              Box.hBox10,
              Text(
                item.shareUser.isEmpty ? item.author : item.shareUser,
                style: Styles.style_9F9EA6_11,
              ),
              Box.hBox10,
              Text(item.niceDate, style: Styles.style_9F9EA6_11),
            ],
          ),
          Box.vBox10,
        ],
      ),
    );
  }

  /// 获取第一行内部边距的Text
  ///[title] 标题
  String getPaddingText(String title ){
    return index <= 2 ? "     $title" : title ;
  }

}