import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_save_state_view.dart';
import 'package:wan/ui/page/home_main_page/widget/banner_widget.dart';

import '../../../routes/routes.dart';
import '../../../util/web_util.dart';
import '../../../widget/pull_smart_refresher.dart';
import '../../../widget/ripple_widegt.dart';
import '../main_page/widget/main_tab_title.dart';
import 'home_main_controller.dart';
import 'widget/main_article_item.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/14 10:39
/// @Description TODO
class HomeMainPage extends GetSaveView<HomeMainController> {
  const HomeMainPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
              child: RefreshWidget<HomeMainController>(
                  child: ListView.builder(
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    itemCount: controller.projectData.length + 2,
                    itemBuilder: (BuildContext context, int index) {
                      ///将Banner装载到ListView中
                      if (index == 0) {
                        return Container(
                          margin:
                          const EdgeInsets.only(top: 10, bottom: 10),
                          width: double.infinity,
                          height: 140,
                          child: BannerWidget(
                            controller.banner,
                            height: 140,
                            onTap: (index) {
                              if (index == 0) {
                                // Get.toNamed(Routes.rankingPage);
                              } else {
                                //WebUtil.toWebPageBanners(controller.banner[index]);
                              }
                            },
                          ),
                        );
                      } else if (index == 1) {
                        ///新增菜单栏
                        return SizedBox(
                          width: double.infinity,
                          child: GridView.builder(
                            shrinkWrap: true,
                            gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 4, //每行显示4个
                                childAspectRatio: 1.0), //显示区域宽高相等
                            itemCount: controller.menu.length,
                            itemBuilder: (context, index) {
                              var type = controller.menu[index].type;
                              return InkWell(
                                onTap: () => {
                                  if(type==0){
                                    //项目
                                    Get.toNamed(Routes.projectPage)
                                  }else if (type == 2)
                                    {
                                      //问答
                                      Get.toNamed(Routes.askPage)
                                    }
                                  else if (type == 3)
                                      {
                                        //导航
                                        Get.toNamed(Routes.classifyPage)
                                      }
                                    else if (type == 1)
                                        {
                                          //教程
                                          Get.toNamed(Routes.coursePage)
                                        }
                                },
                                child: TabTitleIcon(
                                  title: controller.menu[index].title,
                                  icon: controller.menu[index].icon,
                                ),
                              );
                            },
                          ),
                        );
                      } else {
                        ///计算当前显示的真实索引
                        var newIndex = index - 2;

                        ///item列表数据展示
                        return Material(
                          color: Colors.transparent,
                          child: Ripple(
                              onTap: () => WebUtil.toWebPage(
                                  controller.projectData[newIndex],
                                  onResult: (value) {
                                    controller.projectData[newIndex]
                                        .collect = value;
                                  }),
                              child: MainArticleItem(
                                item: controller.projectData[newIndex],
                                index: newIndex,
                              )),
                        );
                      }
                    },
                  )))
        ],
      ));
  }
}
