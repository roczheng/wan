import 'package:flutter/material.dart';
import 'package:wan/base/get/base_page_controller.dart';

import '../../../base/refresher_extension.dart';
import '../../../entity/banners_model.dart';
import '../../../entity/menu_mode.dart';
import '../../../entity/project_entity.dart';
import '../../../res/r.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import '../../../widget/pull_smart_refresher.dart';

/// @Author rozheng
/// @Date 2024/5/14 10:40
/// @Description TODO
class HomeMainController extends BaseGetPageController{

  ///首页数据
  List<ProjectDetail> projectData = [];

  ///首页Banner轮播图
  List<Banners> banner = [];

  ///Banner轮播图控制器
  final PageController pageController = PageController(
    initialPage: 0,
    viewportFraction: 0.8,
  );

  ///当前轮播的索引
  int currentIndex = 1;

  ///是否是第一次加载
  bool isFirst = true;

  List<MenuMode> menu = [
    MenuMode(title: '项目', icon:Icons.backup_table_rounded, type: 0),
    MenuMode(title: '教程', icon:Icons.book, type: 1),
    MenuMode(title: '问答', icon:Icons.question_answer, type: 2),
    MenuMode(title: '导航', icon:Icons.nature_people, type: 3)
  ];

  @override
  void onInit() {
    super.onInit();
    getBanner();
  }

  ///请求首页项目数据
  @override
  void requestData(RefreshController controller,
      {Refresh refresh = Refresh.first}) {
    request.requestHomeArticle(page, success: (data, over) {
      RefreshExtension.onSuccess(controller, refresh, over);

      ///下拉刷新需要清除列表
      if (refresh != Refresh.down) {
        projectData.clear();
      }

      projectData.addAll(data);
      showSuccess(projectData);

      ///为了防止动画每次都加载，所以只需要在第一次出现时加载一次
      update();

    }, fail: (code, msg) {
      showError();
      RefreshExtension.onError(controller, refresh);
    });
  }

  ///请求获取首页Banner图片
  void getBanner() {
    request.getBanner(success: (data) {
      ///添加自定义的积分排行榜图片
      banner.add(Banners(
        imagePath: R.assetsIntegralRanking,
        isAssets: true,
      ));
      banner.addAll(data);

      ///预缓存banner图片
      for (var element in data) {
        if (Get.context != null) {
          ///预缓存图片
          precacheImage(NetworkImage(element.imagePath), Get.context!);
        }
      }
      update();

    });
  }
}