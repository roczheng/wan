import 'package:flutter/material.dart';
import 'package:wan/base/get/getx_controller_inject.dart';
import 'package:get/get.dart';

import '../../../entity/classify_model.dart';

/// @Author rozheng
/// @Date 2024/5/15 11:58
/// @Description TODO
class ClassifyController extends BaseGetController {
  RxList<Classify> classifyData = <Classify>[].obs;
  RxInt currentIndex = 0.obs; //当前选中的index
  RxInt loadStatus = 0.obs; //0加载中 1加载成功
  ///获取分类
  void getClassify() {
    request.getClassify(success: (data) {
      classifyData.value = data;
      loadStatus.value = 1;
      debugPrint('classifyData.value:${classifyData.value}');
    });
  }

  @override
  void onInit() {
    super.onInit();
    getClassify();
  }
}
