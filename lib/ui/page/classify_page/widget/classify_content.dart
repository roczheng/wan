import 'package:flutter/material.dart';
import 'package:wan/res/color.dart';
import 'package:wan/util/web_util.dart';
import '../../../../entity/classify_model.dart';
import '../../../../entity/web_model.dart';
import '../../../../res/decoration_style.dart';
import '../../../../res/shadow_style.dart';
import '../../../../res/style.dart';
import '../../../../routes/routes.dart';
import '../../../../util/toast_utils.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2023/7/9 22:23
/// @Description     for (var item in classifyContentData)
class ClassifyContentItem extends StatelessWidget{
  List<ClassifyModuleDataArticles> classifyContentData = [];
  String title = '';

  ClassifyContentItem({key,required this.classifyContentData,required this.title}):super(key:key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 5),
            decoration: const BoxDecoration(color: ColorStyle.color_24CF5F,borderRadius:BorderRadius.all(Radius.circular(5))),
            height: 40,
            alignment: Alignment.center,
            child: Text(
              title,
              style: Styles.style_white_16_bold,
            ),
          ),
          Box.vBox10,
          Wrap(//流式布局
            spacing: 8.0, // 主轴(水平)方向间距
            runSpacing: 8.0, // 纵轴（垂直）方向间距
            alignment: WrapAlignment.center, //沿主轴方向居中
            children: [
              for (var item in classifyContentData)
               InkWell(
                 onTap: (){
                   //ToastUtils.show("点击了${item.title}");
                   Get.toNamed(Routes.webViewPage, arguments: WebEntity(
                     title: item.title,
                     link: item.link,
                     id: item.id
                   ));
                 },
                 child:  Container(
                   padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                   decoration: DecorationStyle.colorShadowRadius30(),
                   child: Text(
                     item.title,
                     style: Styles.style_black_14,
                   ),
                 ),
               ),
            ],
          )]
      ),
    );
  }

}