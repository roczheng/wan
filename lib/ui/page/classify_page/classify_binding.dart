import 'package:get/get.dart';
import 'package:wan/ui/page/classify_page/classify_controller.dart';

/// @Author rozheng
/// @Date 2024/5/15 12:06
/// @Description TODO
class ClassifyBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ClassifyController());
  }
}
