import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/ui/page/classify_page/classify_controller.dart';
import 'package:get/get.dart';
import 'package:scrollview_observer/scrollview_observer.dart';
import 'package:lottie/lottie.dart';
import 'package:wan/widget/toolbar.dart';

import '../../../res/color.dart';
import '../../../res/decoration_style.dart';
import '../../../res/r.dart';
import '../../../res/style.dart';
import 'widget/classify_content.dart';

/// @Author rozheng
/// @Date 2024/5/15 11:53
/// @Description 导航分类
class ClassifyPage extends GetCommonView<ClassifyController> {
  const ClassifyPage({super.key});

  @override
  Widget build(BuildContext context) {
    ScrollController _scrollControllerTitle = ScrollController();
    ListObserverController _observerControllerTitle =
        ListObserverController(controller: _scrollControllerTitle);

    ScrollController _scrollControllerContent = ScrollController();
    ListObserverController _observerControllerContent =
        ListObserverController(controller: _scrollControllerContent);

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          ToolBar(
            title: "导航",
          ),
          Expanded(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Obx(() => Visibility(
                      visible: controller.loadStatus.value == 1,
                      child: Flex(
                        direction: Axis.horizontal,
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: Container(
                                color: ColorStyle.color_FAFAFB,
                                padding: const EdgeInsets.only(left: 4, right: 4),
                                child: Obx(() => ListViewObserver(
                                  controller: _observerControllerTitle,
                                  child: ListView.builder(
                                    itemBuilder: (context, index) {
                                      return InkWell(
                                        onTap: () {
                                          controller.currentIndex.value =
                                              index;
                                          _observerControllerContent.jumpTo(
                                              index: index);
                                        },
                                        child: Obx(() => Container(
                                          decoration:
                                          DecorationStyle.customize(
                                              controller.currentIndex
                                                  .value ==
                                                  index
                                                  ? ColorStyle
                                                  .color_24CF5F
                                                  : ColorStyle
                                                  .color_24CF5F_20,
                                              6),
                                          height: 40,
                                          margin: const EdgeInsets.only(
                                              top: 2, bottom: 2),
                                          alignment: Alignment.center,
                                          child: Text(
                                            controller
                                                .classifyData[index].name,
                                            style: controller.currentIndex
                                                .value ==
                                                index
                                                ? Styles.style_white_16
                                                : Styles.style_white24_16,
                                          ),
                                        )),
                                      );
                                    },
                                    itemCount: controller.classifyData.length,
                                    controller: _scrollControllerTitle,
                                  ),
                                ))),
                          ),
                          Expanded(
                            flex: 2,
                            child: Container(
                                height: double.infinity,
                                color: Colors.white,
                                child: Obx(
                                      () => ListViewObserver(
                                    onObserve: (resultMap) {
                                      // 打印当前正在显示的第一个子部件
                                      print(
                                          'firstChild.index -- ${resultMap.firstChild?.index}');

                                      if (resultMap.firstChild?.index != null) {
                                        controller.currentIndex.value =
                                            resultMap.firstChild!.index;
                                      }

                                      // 打印当前正在显示的所有子部件下标
                                      print(
                                          'displaying -- ${resultMap.displayingChildIndexList}');
                                    },
                                    controller: _observerControllerContent,
                                    child: ListView.builder(
                                      itemBuilder: (context, index) {
                                        return ClassifyContentItem(
                                          classifyContentData: controller
                                              .classifyData[index].articles,
                                          title:
                                          controller.classifyData[index].name,
                                        );
                                      },
                                      itemCount: controller.classifyData.length,
                                      controller: _scrollControllerContent,
                                    ),
                                  ),
                                )),
                          ),
                        ],
                      ))),

                  ///未加载前显示的动画，加载之后需要隐藏
                  Obx(
                        () => Visibility(
                        visible: controller.loadStatus.value == 0,
                        child: SizedBox(
                          width: 200,
                          height: 200,
                          child: Lottie.asset(R.assetsLottiePageLoading,
                              width: 200, height: 200, animate: true),
                        )),
                  )
                ],
              ))
        ],
      ),
    );
  }
}
