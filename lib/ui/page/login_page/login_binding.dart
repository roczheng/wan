import 'package:get/get.dart';

import 'login_controller.dart';
/// @Author rozheng
/// @Date 2024/5/6 16:17
/// @Description TODO
class LoginBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => LoginController());
  }
}