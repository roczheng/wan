import 'package:flutter/material.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/routes/routes.dart';
import 'package:wan/ui/page/login_page/login_controller.dart';

import '../../../res/button_style.dart';
import '../../../res/color.dart';
import '../../../res/r.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../util/KeyboardUtils.dart';
import 'widget/edit_widget.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/4/30 17:51
/// @Description 登陆页面
class LoginPage extends GetCommonView<LoginController> {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: const BoxDecoration(
          image:DecorationImage(//通常用来设置背景
            image: AssetImage(R.assetsImagesLoginBackground),
              fit:BoxFit.cover
          )
        ),
        child: Column(
         mainAxisAlignment: MainAxisAlignment.center,//主轴居中
          children: [
            ///账户名输入框
            EditWidget(
              iconWidget: const Icon(
                Icons.perm_identity,
                color: Colors.black,
              ),
              hintText: StringStyles.loginAccountNameHint.tr,
              onChanged: (text) => controller
                ..account = text
                ..update(),
            ),
            ///密码输入框
            EditWidget(
              iconWidget: const Icon(Icons.lock_open, color: Colors.black),
              hintText: StringStyles.loginAccountPwdHint.tr,
              passwordType: true,
              onChanged: (text) => controller
                ..password = text
                ..update(),
            ),
            ///登录按钮
            Container(
              width: double.infinity,
              height: 50,
              margin: const EdgeInsets.only(top: 36, left: 25, right: 25),
              decoration: BoxDecoration(
                color: controller.changeShowButton()
                    ? ColorStyle.color_24CF5F
                    : ColorStyle.color_24CF5F_20,
                borderRadius: const BorderRadius.all(Radius.circular(30)),
              ),
              child: TextButton(
                  style: controller.changeShowButton()
                      ? ButtonStyles.getButtonStyle()
                      : ButtonStyles.getTransparentStyle(),
                  onPressed: () {
                    KeyboardUtils.hideKeyboard(context);
                    controller.login();
                  },
                  child: Text(
                    StringStyles.loginButton.tr,
                    style: controller.changeShowButton()
                        ? Styles.style_white_18
                        : Styles.style_white24_18,
                  )),
            ),
            Container(
              width: double.infinity,
              height: 50,
              margin: const EdgeInsets.only(top: 16,left: 25,right: 25),
              decoration: const BoxDecoration(
                color: ColorStyle.color_24CF5F,
                borderRadius: BorderRadius.all(Radius.circular(30)),
              ),
              child: TextButton(
                  style: ButtonStyles.getButtonStyle(),
                  onPressed: () {
                    KeyboardUtils.hideKeyboard(context);
                    Get.toNamed(Routes.registerPage);
                  },
                  child: Text(
                    StringStyles.registerButton.tr,
                    style: Styles.style_white_18
                  )),
              ),
          ],
        ),
      ),
    );
  }
}
