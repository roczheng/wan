import 'package:flutter/cupertino.dart';

/// @Author rozheng
/// @Date 2024/6/10 10:30
/// @Description TODO
class TurnBox extends StatefulWidget {
  double turns;
  int speed;
  Widget child;

   TurnBox(
      {this.turns = .0, //旋转的“圈”数,一圈为360度，如0.25圈即90度
      this.speed = 200, //过渡动画执行的总时长
      required this.child,super.key});

  @override
  _TurnBoxState createState() => _TurnBoxState();
}

class _TurnBoxState extends State<TurnBox> with SingleTickerProviderStateMixin {
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
        vsync: this, lowerBound: -double.infinity, upperBound: double.infinity);
    _controller.value = widget.turns;
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RotationTransition(
      turns: _controller,
      child: widget.child,
    );
  }

  @override
  void didUpdateWidget(covariant TurnBox oldWidget) {
    super.didUpdateWidget(oldWidget);
    // 旋转角度发送变化时执行过度动画
    if (oldWidget.turns != widget.turns) {
      _controller.animateTo(widget.turns,
          duration: Duration(microseconds: widget.speed ?? 200),
          curve: Curves.easeOut); //设置时间曲线，以缓慢开始，然后快速结束的方式执行
    }
  }
}
