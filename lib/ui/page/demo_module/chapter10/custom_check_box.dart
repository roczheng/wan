import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// @Author rozheng
/// @Date 2024/6/10 16:38
/// @Description TODO
class CustomCheckbox extends LeafRenderObjectWidget{//LeafRenderObjectWidget
  const CustomCheckbox({
    Key? key,
    this.strokeWidth = 2.0,
    this.value = false,
    this.strokeColor = Colors.white,
    this.fillColor = Colors.blue,
    this.radius = 2.0,
    this.onChanged,
  }) : super(key: key);

  final double strokeWidth; // “勾”的线条宽度
  final Color strokeColor; // “勾”的线条宽度
  final Color? fillColor; // 填充颜色
  final bool value; //选中状态
  final double radius; // 圆角
  final ValueChanged<bool>? onChanged;

  @override
  RenderObject createRenderObject(BuildContext context) {
    // TODO: implement createRenderObject
    throw UnimplementedError();
  } // 选中状态发生改变后的回调

  // @override
  // RenderObject createRenderObject(BuildContext context) {
  //   // return RenderCustomCheckbox(
  //   //   strokeWidth,
  //   //   strokeColor,
  //   //   fillColor ?? Theme.of(context).primaryColor,
  //   //   value,
  //   //   radius,
  //   //   onChanged,
  //   // );
  // }

  // @override
  // void updateRenderObject(context, RenderCustomCheckbox renderObject) {
  //   if (renderObject.value != value) {
  //     renderObject.animationStatus =
  //     value ? AnimationStatus.forward : AnimationStatus.reverse;
  //   }
  //   renderObject
  //     ..strokeWidth = strokeWidth
  //     ..strokeColor = strokeColor
  //     ..fillColor = fillColor ?? Theme.of(context).primaryColor
  //     ..radius = radius
  //     ..value = value
  //     ..onChanged = onChanged;
  // }


}