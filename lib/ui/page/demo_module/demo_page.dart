import 'package:flutter/material.dart';
import 'package:wan/ui/page/demo_module/chapter10/turn_box_route.dart';
import 'package:wan/ui/page/demo_module/my_widget.dart';
import 'package:wan/ui/page/demo_module/provider/demo/provider_route.dart';
import 'package:wan/ui/page/demo_module/zoom_route.dart';
import 'package:wan/widget/toolbar.dart';

import 'InheritedWidget/inherited_widget_test_route.dart';

/// @Author rozheng
/// @Date 2024/5/29 18:02
/// @Description TODO
class DemoPage extends StatelessWidget {
  const DemoPage({super.key});

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: Column(
        children: [
          ToolBar(
            title: "demo",
          ),
          ListTile(
            title: Text("数据共享（InheritedWidget）"),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => InheritedWidgetTestRoute()),
              )
            },
          ),
          ListTile(
            title: Text("数据共享（自实现Provider）"),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ProviderRoute()),
              )
            },
          ),
          ListTile(
            title: Text("Zoom 拖动"),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => ZoomRoute()),
              )
            },
          ),
          ListTile(
            title: Text("TurnBoxRoute"),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => TurnBoxRoute()),
              )
            },
          ),
          ListTile(
            title: Text("clipWidget"),
            onTap: () => {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => MyWidget()),
              )
            },
          ),
        ],
      ),
    );
  }
}
