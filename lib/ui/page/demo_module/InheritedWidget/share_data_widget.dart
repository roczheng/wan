import 'package:flutter/material.dart';

/// @Author rozheng
/// @Date 2024/5/29 18:07
/// @Description TODO
class ShareDataWidget extends InheritedWidget {
  ShareDataWidget({
    Key? key,
    required this.data,
    required Widget child,
  }) : super(key: key, child: child);

  final int data; //需要在子树中共享的数据，保存点击次数

  //定义一个便捷方法，方便子树中的widget获取共享数据
  static ShareDataWidget? of(BuildContext context) {
    //return context.dependOnInheritedWidgetOfExactType<ShareDataWidget>();//注册依赖关系，会回调用子组件的didChangeDependencies方法
    return context.getElementForInheritedWidgetOfExactType<ShareDataWidget>()!.widget as ShareDataWidget;//会触发整个页面build
  }
  //该回调决定当data发生变化时，是否通知子树中依赖data的Widget重新build
  @override
  bool updateShouldNotify(ShareDataWidget old) {
    return old.data != data;
  }
}