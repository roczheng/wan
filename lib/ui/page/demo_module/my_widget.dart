import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

/// @Author rozheng
/// @Date 2024/6/10 18:53
/// @Description TODO
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('GestureDetector Example'),
        ),
        body: const Center(
          child: MyWidget(),
        ),
      ),
    );
  }
}

class MyWidget extends StatefulWidget {
  const MyWidget({Key? key}) : super(key: key);

  @override
  State<MyWidget> createState() => _MyWidgetState();
}

class _MyWidgetState extends State<MyWidget> {
  final _scaleRecognizer = ScaleGestureRecognizer();
  double _scale = 1.0;
  Offset _panOffset = Offset.zero;

  @override
  void initState() {
    super.initState();
    _scaleRecognizer
      ..onStart = _handleScaleStart
      ..onUpdate = _handleScaleUpdate
      ..onEnd = _handleScaleEnd;
  }

  @override
  void dispose() {
    _scaleRecognizer.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return RawGestureDetector(
      gestures: {
        ScaleGestureRecognizer:
        GestureRecognizerFactoryWithHandlers<ScaleGestureRecognizer>(
              () => _scaleRecognizer,
              (ScaleGestureRecognizer instance) {},
        ),
      },
      child: Transform.scale(
        scale: _scale,
        child: Transform.translate(
          offset: _panOffset,
          child: Image.network("https://img1.baidu.com/it/u=3580669984,532975628&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=800"),
        ),
      ),
    );
  }

  void _handleScaleStart(ScaleStartDetails details) {
    print('缩放开始');
  }

  void _handleScaleUpdate(ScaleUpdateDetails details) {
    setState(() {
      _scale = details.scale;
      _panOffset = details.localFocalPoint;
    });
    print('缩放更新，当前缩放值：$_scale');
    print('拖动更新，当前偏移值：$_panOffset');
  }

  void _handleScaleEnd(ScaleEndDetails details) {
    print('缩放结束');
  }
}
