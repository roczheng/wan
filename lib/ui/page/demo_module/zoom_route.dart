import 'package:flutter/material.dart';

/// @Author rozheng
/// @Date 2024/6/7 12:00
/// @Description TODO
class ZoomRoute extends StatefulWidget {
  const ZoomRoute({super.key});

  @override
  State<ZoomRoute> createState() => _ZoomRouteState();
}

class _ZoomRouteState extends State<ZoomRoute> {
  Matrix4 _transform = Matrix4.identity();
  Offset _lastPanPosition = Offset.zero;

  void _handleScaleStart(ScaleStartDetails details) {
    // 缩放开始时，重置变换矩阵
    _transform = Matrix4.identity();
  }

  void _handleScaleUpdate(ScaleUpdateDetails details) {
    // 缩放更新时，根据缩放比例更新变换矩阵
    final matrix = Matrix4.diagonal3Values(details.scale, details.scale, 1.0);
    _transform = matrix;

    setState(() {});
  }

  void _handlePanStart(DragStartDetails details) {
    // 拖动开始时，记录起始位置
    _lastPanPosition = details.globalPosition;
  }

  void _handlePanUpdate(DragUpdateDetails details) {
    // 拖动更新时，根据拖动距离更新变换矩阵
    final delta = details.globalPosition - _lastPanPosition;
    final translation = Matrix4.translationValues(delta.dx, delta.dy, 0);
    _transform = _transform * translation;
    _lastPanPosition = details.globalPosition;

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onScaleStart: _handleScaleStart,
      onScaleUpdate: _handleScaleUpdate,
      // onPanStart: _handlePanStart,
      // onPanUpdate: _handlePanUpdate,
      child: Transform(
        transform: _transform,
        child: Container(
          width: 200,
          height: 200,
          color: Colors.blue,
          child: Center(
            child: Text(
              '双指缩放和单指移动',
              style: TextStyle(color: Colors.white, fontSize: 24),
            ),
          ),
        ),
      ),
    );
  }
}
