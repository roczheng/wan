import 'package:get/get.dart';
/// @Author rozheng
/// @Date 2024/4/30 17:58
/// @Description TODO
class Controller extends GetxController{
  var count = 0;
  void increment() {
    count++;
    update();
  }
}