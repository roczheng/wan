/// @Author rozheng
/// @Date 2024/5/30 12:17
/// @Description TODO
class Item {
  Item(this.price, this.count);
  double price; //商品单价
  int count; // 商品份数
//... 省略其他属性
}