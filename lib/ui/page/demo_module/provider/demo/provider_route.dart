import 'package:flutter/material.dart';
import 'package:wan/ui/page/demo_module/provider/consumer.dart';

import '../change_notifier_provider.dart';
import 'cart_model.dart';
import 'item.dart';

/// @Author rozheng
/// @Date 2024/5/30 12:18
/// @Description TODO
class ProviderRoute extends StatefulWidget {
  @override
  _ProviderRouteState createState() => _ProviderRouteState();
}

class _ProviderRouteState extends State<ProviderRoute> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: ChangeNotifierProvider<CartModel>(
        data: CartModel(),
        child: Builder(builder: (context) {
          return Column(
            children: <Widget>[
              Consumer<CartModel>(builder: (context,cart)=>Text("总价: ${cart?.totalPrice}")),
              Builder(builder: (context) {
                print("ElevatedButton build"); //在后面优化部分会用到
                return ElevatedButton(
                  child: Text("添加商品"),
                  onPressed: () {
                    //给购物车中添加商品，添加后总价会更新
                    ChangeNotifierProvider.of<CartModel>(context,listen: false)
                        ?.add(Item(20.0, 1));
                  },
                );
              }),
            ],
          );
        }),
      ),
    );
  }
}
