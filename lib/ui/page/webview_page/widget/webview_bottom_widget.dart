import 'package:flutter/material.dart';

import '../../../../base/get/get_common_view.dart';
import '../../../../res/color.dart';
import '../../../../res/r.dart';
import '../../../../res/shadow_style.dart';
import '../../../../res/string_styles.dart';
import '../../../../res/style.dart';
import '../../../../util/navigate_util.dart';
import '../../../../util/toast_utils.dart';
import '../webview_controller.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/svg.dart';


/// @Author rozheng
/// @Date 2023/5/11 20:38
/// @Description TODO
class WebViewBottomWidget extends GetCommonView<WebController> {
  const WebViewBottomWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      decoration: ShadowStyle.white12Circle(radius: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Box.hBox20,
          Expanded(
              child: GestureDetector(
                onTap: () => ToastUtils.show(StringStyles.webNotComment.tr),
                child: Container(
                    padding: const EdgeInsets.all(8),
                    decoration: const BoxDecoration(
                      color: ColorStyle.colorShadow,
                      borderRadius: BorderRadius.all(Radius.circular(20)),
                    ),
                    child: Text(
                      StringStyles.webEditHint.tr,
                      style: Styles.style_B8C0D4_14,
                    )),
              )),
          Box.hBox20,
          InkWell(
            onTap: () => controller.collectArticle(),
            child: Obx(() => SvgPicture.asset(
              controller.isCollect.value ? R.assetsImagesCollect : R.assetsImagesCollectQuit,
              width: 24,
            )),
          ),
          Box.hBox20,
          InkWell(
              onTap: ()=> ToastUtils.show(StringStyles.notSupportLikes.tr),
              child: const Icon(
                Icons.thumb_up_alt_outlined,
                color: ColorStyle.color_24CF5F,
                size: 24,
              )),
          Box.hBox20,
        InkWell(
          onTap: () => NavigateUtil.launchInBrowser(controller.detail.link),
          child: const Icon(
            Icons.public,
            color: Colors.blue,
            size: 24,
          ),
        ),
          Box.hBox20,
        ],
      ),
    );
  }
}