
import 'package:get/get.dart';

import 'webview_controller.dart';
/// @Author rozheng
/// @Date 2023/5/11 21:01
/// @Description TODO

class WebViewBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => WebController());
  }
}