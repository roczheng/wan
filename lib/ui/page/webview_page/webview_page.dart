import 'package:flutter/material.dart';

import '../../../base/get/get_common_view.dart';
import 'package:get/get.dart';

import '../../../res/color.dart';
import '../../../res/r.dart';
import 'package:lottie/lottie.dart';
import 'package:flutter_svg/svg.dart';

import '../../../widget/toolbar.dart';
import 'webview_controller.dart';
import 'widget/webview_bottom_widget.dart';
import 'package:webview_flutter/webview_flutter.dart';

/// @Author rozheng
/// @Date 2023/5/11 20:35
/// @Description TODO,目前有问题，WebView没有即时释放掉
class WebViewPage extends GetCommonView<WebController> {
  const WebViewPage({super.key});

  @override
  Widget build(BuildContext context) {


    return PopScope(
        canPop: false,
        onPopInvoked: (didPop) {
          debugPrint("进来了");
          if ( didPop ) return;         // really exit
          else {
            /// 拦截用户返回，返回时携带参数
            Get.back(result: controller.isCollect.value);
          }
          //return true;
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Column(
            children: [
              Stack(
                children: [
                  ToolBar(
                    backColor: ColorStyle.color_474747,
                    backOnTap: () =>
                        Get.back(result: '${controller.isCollect}'),
                    title: controller.detail.title,
                  ),
                  Positioned(
                    right: 20,
                    bottom: 11,
                    width: 24,
                    height: 24,
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(20),
                        ),
                        splashColor: ColorStyle.color_E2E3E8_66,
                        onTap: () {
                          //Share.share(controller.detail.title + controller.detail.link);
                        },
                        child: SvgPicture.asset(
                          R.assetsImagesShare,
                          width: 16,
                          height: 16,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: Stack(
                  children: [
                    WebViewWidget(controller: controller.webController),
                    Obx(() => Visibility(
                          visible: controller.collectAtState.value,
                          child: Positioned(
                              left: 0,
                              right: 0,
                              top: 0,
                              bottom: 0,
                              child: Lottie.asset(
                                R.assetsLottieCollect,
                                animate: controller.collectAtState.value,
                              )),
                        )),
                    Obx(() => Visibility(
                          visible: controller.progress < 1,
                          child: LinearProgressIndicator(
                            minHeight: 2,
                            backgroundColor: ColorStyle.color_F9F9F9,
                            color: ColorStyle.color_24CF5F,
                            value: controller.progress.value,
                          ),
                        )),
                  ],
                ),
              ),
              Visibility(
                visible: controller.detail.id > 0,
                child: const WebViewBottomWidget(),
              ),
            ],
          ),
        ));
  }
}
