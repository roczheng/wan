import 'package:flutter/material.dart';

import '../../../base/get/getx_controller_inject.dart';
import 'package:get/get.dart';
import '../../../entity/web_model.dart';
import '../../../res/string_styles.dart';
import '../../../util/toast_utils.dart';
import 'package:webview_flutter/webview_flutter.dart';

/// @Author rozheng
/// @Date 2023/5/11 20:36
/// @Description TODO
class WebController extends BaseGetController {
  ///加载URL
  WebEntity detail = Get.arguments;

  ///进度条
  var progress = 0.0.obs;

  ///是否点赞
  var isCollect = false.obs;

  ///控制收藏的取消与结束
  var collectAtState = false.obs;

  late final WebViewController webController;

  @override
  void onInit() {
    super.onInit();
    isCollect.value = detail.isCollect;

    final WebViewController webController = WebViewController();
    webController
      ..setJavaScriptMode(JavaScriptMode.unrestricted)
      ..setBackgroundColor(const Color(0x00000000))
      ..setNavigationDelegate(
        NavigationDelegate(
          onProgress: (int progress) {
            this.progress.value = (progress / 100).toDouble();
            debugPrint('WebView is loading (progress : $progress%)');
          },
          onPageStarted: (String url) {
            debugPrint('Page started loading: $url');
          },
          onPageFinished: (String url) {
            debugPrint('Page finished loading: $url');
          },
          onWebResourceError: (WebResourceError error) {
            debugPrint(
                '''Page resource error: code: ${error.errorCode} description: ${error.description}
              errorType: ${error.errorType} isForMainFrame: ${error.isForMainFrame}
          ''');
          },
          onUrlChange: (UrlChange change) {
            debugPrint('url change to ${change.url}');
          },
          onHttpAuthRequest: (HttpAuthRequest request) {},
        ),
      )
      ..loadRequest(Uri.parse(detail.link));
    this.webController = webController;
  }

  ///收藏&取消收藏
  ///注意此处，从收藏进入取消收藏的ID是originId
  collectArticle() {
    if (!isCollect.value) {
      collectAtState.value = true;
      Future.delayed(const Duration(milliseconds: 900)).then((value) {
        collectAtState.value = false;
      });
    }

    request.collectArticle(
        isCollect.value && detail.originId != 0 ? detail.originId : detail.id,
        isCollect: isCollect.value, success: (data) {
      ToastUtils.show(isCollect.value
          ? StringStyles.collectQuit.tr
          : StringStyles.collectSuccess.tr);
      isCollect.value = !isCollect.value;
      update();
    });
  }

  @override
  void onClose() {
    // TODO: implement onClose
    super.onClose();
    print("页面关闭了");
    webController.loadRequest(Uri.parse('about:blank'));
  }
}
