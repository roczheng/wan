import 'package:flutter/material.dart';

import '../../../../entity/project_entity.dart';
import '../../../../res/color.dart';
import '../../../../res/style.dart';
import '../../../../util/web_util.dart';

/// @Author rozheng
/// @Date 2024/5/27 16:39
/// @Description TODO
class HistoryItemWidget extends StatelessWidget {

  ProjectDetail detail;

  Function(bool) onResult;

  HistoryItemWidget({super.key, required this.detail, required this.onResult});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => WebUtil.toWebPage(detail, onResult: onResult),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16,vertical: 5),
        padding: EdgeInsets.all(10),
        decoration: const BoxDecoration(
          color: ColorStyle.color_F4F6FA,
          borderRadius: BorderRadius.all(Radius.circular(10))
        ),
        child: Row(
          children: [
            Visibility(
              visible: detail.envelopePic.isNotEmpty,
              child: Padding(
                padding: EdgeInsets.only(right: 10),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(5),
                  child: Image.network(
                    detail.envelopePic,
                    fit: BoxFit.cover,
                    width: 82,
                    height: 102,
                  ),
                ),
              ),
            ),
            Expanded(child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  detail.title,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                  style: Styles.style_black_16_bold,
                ),
                Box.vBox10,
                Visibility(
                  visible: detail.desc.isNotEmpty,
                  child: Text(
                    detail.desc,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: Styles.style_9F9EA6_14,
                  ),
                ),
                Box.vBox5,
                Text(
                  detail.superChapterName,
                  style: Styles.style_FE8C28_11,
                ),
                Box.vBox5,
               Row(
                 mainAxisAlignment: MainAxisAlignment.end,
                 children: [ Text(detail.niceDate, style: Styles.style_9F9EA6_11),],
               ),
                Box.vBox3,
              ],
            ))
          ],
        ),
      ),
    );
  }
}