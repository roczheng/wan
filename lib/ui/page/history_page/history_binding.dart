import 'package:get/get.dart';
import 'package:wan/ui/page/history_page/history_controller.dart';
/// @Author rozheng
/// @Date 2024/5/27 16:31
/// @Description TODO
class HistoryBinding extends Bindings{
  @override
  void dependencies() {
      Get.lazyPut(() => HistoryController());
  }
}