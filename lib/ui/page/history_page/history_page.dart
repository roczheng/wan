import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/ui/page/history_page/history_controller.dart';

import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../widget/toolbar.dart';
import 'package:get/get.dart';

import 'widget/history_item_widget.dart';

/// @Author rozheng
/// @Date 2024/5/27 16:31
/// @Description TODO
class HistoryPage extends GetCommonView<HistoryController>{
  const HistoryPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        child: Column(
          children: [
            ToolBar(
              title: StringStyles.historyTitle.tr,
            ),
            DividerStyle.divider1HalfPadding20,

            ///浏览历史
            Expanded(
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  itemCount: controller.historyList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return HistoryItemWidget(
                      detail: controller.historyList[index],
                      onResult: (value) =>  controller.historyList[index].collect = value,
                    );
                  },
                )),
          ],
        ),
      ),
    );
  }

}