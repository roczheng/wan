import 'package:wan/base/get/getx_controller_inject.dart';

import '../../../entity/project_entity.dart';
import '../../../util/save/sp_util.dart';

/// @Author rozheng
/// @Date 2024/5/27 16:32
/// @Description TODO
class HistoryController extends BaseGetController{
  ///历史记录
  List<ProjectDetail> historyList = [];

  @override
  void onInit() {
    super.onInit();
    historyList = SpUtil.getBrowseHistoryModel();
    print("listSize=>"+historyList.length.toString());
    update();
  }

}