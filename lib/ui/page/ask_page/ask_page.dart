import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_save_state_view.dart';

import '../../../res/string_styles.dart';
import '../../../widget/pull_smart_refresher.dart';
import '../../../widget/toolbar.dart';
import 'ask_controller.dart';
import 'widget/ask_item_widget.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/14 19:34
/// @Description TODO
class AskPage extends GetSaveView<AskController> {
  const AskPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:  Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          ToolBar(title: StringStyles.tabAsk.tr),
          Expanded(child: RefreshWidget<AskController>(
            child: ListView.builder(
              padding: EdgeInsets.zero,
              shrinkWrap: true,

              ///该属性表示是否根据子组件的总长度来设置ListView的长度，默认值为false
              ///。默认情况下，ListView会在滚动方向尽可能多的占用空间。
              ///当ListView在一个无边界(滚动方向上)的容器中时，shrinkWrap必须为true
              itemCount: controller.projectData.length,
              itemBuilder: (BuildContext context, int index) {
                return AskListItem(
                  controller.projectData[index],
                  onResult: (value) {
                    controller.projectData[index].collect = value;
                  },
                );
              },
            ),
          )),
        ],
      ));
  }
}
