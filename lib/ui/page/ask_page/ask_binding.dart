import 'package:get/get.dart';
import 'package:wan/ui/page/ask_page/ask_controller.dart';

/// @Author rozheng
/// @Date 2024/5/15 11:34
/// @Description TODO
class AskBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AskController());
  }

}