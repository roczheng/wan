import 'package:flutter/material.dart';
import 'package:wan/base/get/getx_controller_inject.dart';

import '../../../entity/user_entity.dart';
import '../../../event/event.dart';
import '../../../event/event_bus.dart';
import '../../../routes/routes.dart';
import '../../../util/permission.dart';
import '../../../util/save/sp_util.dart';
import 'package:image_picker/image_picker.dart';
import 'package:get/get.dart';

import 'package:image_cropper/image_cropper.dart';

/**
 * @Author: rozheng
 * @Date: 2024/6/2 18:29
 * @Description:
 */
class PersonalInfoController extends BaseGetController {
  ///用户信息
  late UserEntity userInfo;

  String imagePath = "";

  @override
  void onInit() {
    super.onInit();
    var info = SpUtil.getUserInfo();
    if (info != null) {
      userInfo = info;
      update();
    }
    bus.on(Event.infoUpdate, onUserInfoChanged);
  }

  @override
  void dispose() {
    bus.off(Event.infoUpdate, onUserInfoChanged);
  }

  void onUserInfoChanged(e) {
    print("收到头像更新啦${e}");
    userInfo = e;
    update();
  }

  openCamera() {
    PermissionRequest.sendPermission((value) async {
      if (value) {
        var image = await ImagePicker().pickImage(source: ImageSource.camera);
        if (image != null) {
          print("file--->${image.path}");
          //跳裁剪
          Get.toNamed(Routes.clipPage, arguments: {"path": image.path});
        }
        update();
      }
    });
  }

  openGallery() async {
    PermissionRequest.sendPermission((value) async {
      if (value) {
        var image = await ImagePicker().pickImage(source: ImageSource.gallery);
        if (image != null) {
          print("file--->${image.path}");

          ///Get.toNamed(Routes.clipPage, arguments: {"path": image.path});
          CroppedFile? croppedFile = await ImageCropper().cropImage(
            sourcePath: image.path,
            uiSettings: [
              AndroidUiSettings(
                  toolbarTitle: 'Cropper',
                  toolbarColor: Get.theme.primaryColor,
                  toolbarWidgetColor: Colors.white,
                  hideBottomControls: true,
                  initAspectRatio:  CropAspectRatioPreset.square,
                  aspectRatioPresets: [
                    CropAspectRatioPreset.original,
                    CropAspectRatioPreset.square,
                    ///CropAspectRatioPresetCustom(),
                  ],
                  ),
              IOSUiSettings(
                title: 'Cropper',
                aspectRatioPresets: [
                  CropAspectRatioPreset.original,
                  CropAspectRatioPreset.square,
                  CropAspectRatioPresetCustom(),
                  // IMPORTANT: iOS supports only one custom aspect ratio in preset list
                ],
              ),
              WebUiSettings(
                context: Get.context!!,
              ),
            ],
          );

          print("file path-->${croppedFile!.path}");
          userInfo.icon = croppedFile!.path;
          SpUtil.putUserInfo(userInfo);
          bus.emit(Event.infoUpdate, userInfo);
        }
        update();
      }
    });
  }
}

class CropAspectRatioPresetCustom implements CropAspectRatioPresetData {
  @override
  (int, int)? get data => (2, 3);

  @override
  String get name => '2x3 (customized)';
}
