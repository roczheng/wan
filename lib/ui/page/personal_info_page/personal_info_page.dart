import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/res/string_styles.dart';
import 'package:wan/widget/toolbar.dart';

import '../../../res/shadow_style.dart';
import '../../../res/style.dart';
import '../me_page/widget/head_circle_widget.dart';
import 'personal_info_controller.dart';
import 'package:get/get.dart';

/**
 * @Author: rozheng
 * @Date: 2024/6/2 18:28
 * @Description:
 */
class PersonalInfoPage extends GetCommonView<PersonalInfoController> {
  const PersonalInfoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          ToolBar(
            title: StringStyles.homeUserInfo.tr,
          ),
          Container(
            alignment: Alignment.center,
            height: 200,
            child: GestureDetector(
              onTap: () => {
                ///打开底部弹出框
                _showModalBottomSheet(context)
              },
              child: Container(
                margin: const EdgeInsets.only(top: kTextTabBarHeight),
                decoration: ShadowStyle.black12Circle40(),
                child: HeadCircleWidget(
                  width: 100,
                  height: 100,
                  imagePath: controller.userInfo.icon,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(StringStyles.userNickname.tr,
                    style: Styles.style_1A2F51_18),
                Text(controller.userInfo.nickname,
                    style: Styles.style_1A2F51_18)
              ],
            ),
          ),
          DividerStyle.divider1HalfPadding20,
        ],
      ),
    );
  }

  /// 弹出底部菜单列表模态对话框选择主题
  Future<int?> _showModalBottomSheet(BuildContext context) {
    return showModalBottomSheet<int>(
      context: context,
      builder: (BuildContext context) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Box.vBox20,
            const Text(
              "更换头像",
              style: Styles.style_black_16_bold,
            ),
            Box.vBox20,
            InkWell(
              onTap: () => {controller.openCamera(), Navigator.pop(context)},
              child: Container(
                height: 50,
                alignment: Alignment.center,
                child: const Text(
                  "拍照",
                  style: Styles.style_1A2F51_18,
                ),
              ),
            ),
            DividerStyle.divider1HalfPadding20,
            InkWell(
              onTap: () => {controller.openGallery(),Navigator.pop(context)},
              child: Container(
                height: 50,
                alignment: Alignment.center,
                child: const Text(
                  "相册",
                  style: Styles.style_1A2F51_18,
                ),
              ),
            )
          ],
        );
      },
    );
  }
}
