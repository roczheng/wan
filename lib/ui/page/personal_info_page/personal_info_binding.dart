import 'package:get/get.dart';
import 'package:wan/ui/page/personal_info_page/personal_info_controller.dart';
/**
 * @Author: rozheng
 * @Date: 2024/6/2 18:40
 * @Description:
 */
class PersonalInfoBinding extends Bindings{
  @override
  void dependencies() {
   Get.lazyPut(() => PersonalInfoController());
  }
}