import 'package:get/get.dart';
import 'package:wan/ui/page/splash_page/splash_controller.dart';

/// @Author rozheng
/// @Date 2024/5/8 16:54
/// @Description TODO
class SplashBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SplashController());
  }
}
