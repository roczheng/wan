import 'package:flutter/material.dart';
import 'package:wan/ui/page/splash_page/splash_controller.dart';

import '../../../base/get/get_common_view.dart';
import '../../../res/r.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../routes/routes.dart';
import '../../../util/save/sp_util.dart';
import '../../../util/screen_util.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/8 16:51
/// @Description 启动页
class SplashPage extends GetCommonView<SplashController> {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    ScreenUtil.removeSystemTransparent(context);

    ///预缓存背景图片
    precacheImage(const AssetImage(R.assetsImagesLoginBackground), context);
    return AnimatedOpacity(
      opacity: controller.opacityLevel,
      duration: const Duration(milliseconds: 2000),
      onEnd: () {
        Get.offNamed(
            SpUtil.getUserInfo() == null ? Routes.loginPage : Routes.mainPage);
      },
      child: Container(
        margin: const EdgeInsets.only(top: 120),
        child: Column(
          children: [
            Image.asset(
              R.assetsImagesApplication,
              fit: BoxFit.fitWidth,
              width: 110,
              height: 110,
            ),
            Container(
              margin: const EdgeInsets.only(top: 16),
              child: Text(
                StringStyles.appName.tr,
                style: Styles.style_black_36,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
