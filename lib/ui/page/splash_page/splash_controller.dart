import '../../../base/get/getx_controller_inject.dart';

/// @Author rozheng
/// @Date 2024/5/8 16:55
/// @Description TODO
class SplashController extends BaseGetController {
  ///用来控制动画的状态
  double opacityLevel = 0.0;

  @override
  void onInit() {
    super.onInit();
    lazyInitAnim();
  }

  lazyInitAnim() {
    Future.delayed(const Duration(milliseconds: 200), () {
      opacityLevel = 1.0;
      update();
    });
  }
}
