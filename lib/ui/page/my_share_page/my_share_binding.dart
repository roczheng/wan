import 'package:get/get.dart';
import 'package:wan/ui/page/square_page/square_controller.dart';

import 'my_share_controller.dart';
/// @Author rozheng
/// @Date 2024/5/13 15:00
/// @Description TODO
class MyShareBinding extends Bindings{
  @override
  void dependencies() {
      Get.lazyPut(() =>  MyShareController());
  }

}