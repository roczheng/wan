import 'package:flutter/material.dart';
import '../../../../../res/style.dart';
import '../../../../entity/my_share_entity.dart';
import '../../../../res/color.dart';
import '../../../../res/decoration_style.dart';
import '../../../../res/shadow_style.dart';

/// @Author rozheng
/// @Date 2024/5/7 22:18
/// @Description TODO
class ShareMyArticleItem extends StatelessWidget {
  ///列表数据
  MyShareEntityShareArticlesDatas item;
  GestureTapCallback? gestureTapCallback;
  GestureTapCallback? onTap;


  ShareMyArticleItem({super.key, required this.item,this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: ShadowStyle.white12Circle(),
      margin: const EdgeInsets.only(left: 16, top: 4, bottom: 4, right: 16),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        children: [
          Expanded(child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Box.vBox10,
              Text(
                item.title,
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
                style: Styles.style_black_16_bold,
              ),
              Box.vBox10,
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    item.shareUser.isEmpty ? item.author : item.shareUser,
                    style: Styles.style_9F9EA6_11,
                  ),
                  Box.hBox10,
                  Text(item.niceDate, style: Styles.style_9F9EA6_11),
                ],
              ),
              Box.vBox10,
              DividerStyle.divider1Half
            ],
          )),
           InkWell(
            splashColor: ColorStyle.color_E2E3E8_66,
            onTap: onTap,
            child: const Icon(Icons.delete),
          )
        ],
      ),
    );
  }
}
