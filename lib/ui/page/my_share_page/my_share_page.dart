import 'package:flutter/material.dart';
import 'package:wan/base/get/get_extension.dart';
import 'package:wan/base/get/get_save_state_view.dart';
import 'package:wan/ui/page/my_share_page/my_share_controller.dart';
import 'package:wan/ui/page/square_page/square_controller.dart';
import 'package:wan/widget/ripple_widegt.dart';
import '../../../res/style.dart';
import '../../../widget/pull_smart_refresher.dart';
import '../../../widget/toolbar.dart';
import 'widget/share_my_article_item.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/13 15:00
/// @Description TODO
class MySharePage extends GetSaveView<MyShareController> {
  const MySharePage({super.key});

  @override
  Widget build(BuildContext context) {
    print("进来了");
    return Scaffold(
      backgroundColor: Colors.white,
      body:Column(
        children: [
          ToolBar(
            title: "我的分享",
          ),
          DividerStyle.divider1Half,
          Expanded(
              child: RefreshWidget<MyShareController>(
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  itemCount: controller.projectData.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Ripple(
                        onTap: () => {print("跳转详情页面")},
                        child: ShareMyArticleItem(
                          item: controller.projectData[index],
                          onTap: () => {
                            Get.showDialog(
                                title: '提示',
                                content: '是否确定删除？',
                                nextTap: () => {
                                  controller.deleteShare(
                                      controller.projectData[index].id)
                                })
                          },
                        ));
                  },
                ),
              ))
        ],
      )
    );
  }
}
