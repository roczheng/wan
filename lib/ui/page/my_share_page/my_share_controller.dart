import 'package:wan/base/get/base_page_controller.dart';
import 'package:wan/util/toast_utils.dart';

import '../../../base/refresher_extension.dart';
import '../../../entity/my_share_entity.dart';
import '../../../entity/project_entity.dart';
import '../../../widget/pull_smart_refresher.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

/// @Author rozheng
/// @Date 2024/5/13 15:00
/// @Description TODO
class MyShareController extends BaseGetPageController {
  List<MyShareEntityShareArticlesDatas> projectData = [];

  @override
  void requestData(RefreshController controller,
      {Refresh refresh = Refresh.first}) {
    print("开始请求我的分享");
    request.requestMyShareModule(page, success: (data, over) {
      RefreshExtension.onSuccess(controller, refresh, over);
      ///下拉刷新需要清除列表
      if (refresh != Refresh.down) {
        projectData.clear();
      }
      projectData.addAll(data);
      showSuccess(projectData);
      update();
    }, fail: (code, msg) {
      showError();
      RefreshExtension.onError(controller, refresh);
    });
  }

  deleteShare(int id) {
    request.deleteShare(id, success: (bool) {
      if (bool) {
        ToastUtils.show("删除成功");
        //刷新页面
        onLoadRefresh(controller);
      } else {
        ToastUtils.show("删除失败");
      }
    }, fail: (code, msg) {
      ToastUtils.show(msg);
    });
  }
}
