import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_save_state_view.dart';
import 'package:wan/res/style.dart';
import 'package:wan/ui/page/wechat_public_child_page/wechat_public_child_page.dart';
import 'package:wan/ui/page/wechat_public_page/wechat_public_controller.dart';
import 'package:wan/widget/toolbar.dart';

import '../../../base/keep_alive_wrapper.dart';
import '../../../res/color.dart';
import '../../../res/string_styles.dart';
import 'package:get/get.dart';

import '../wechat_public_child_page/wechat_public_child_controller.dart';

/// @Author rozheng
/// @Date 2024/5/15 11:38
/// @Description 公众号列表
class WeChatPublicPage extends GetSaveView<WeChatPublicController> {
  const WeChatPublicPage({super.key});

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: controller.wechatPublic.length,
        child: Column(
          children: [
            TabBar(
                tabAlignment: TabAlignment.start,
                isScrollable: true,
                //是否可以滑动
                labelColor: Get.theme.primaryColor,
                //标签颜色
                unselectedLabelColor: Colors.black38,

                labelStyle: Styles.style_black_16_bold,
                unselectedLabelStyle: Styles.style_FFAE2E_16,

                indicatorColor:Get.theme.primaryColor,
                tabs: controller.wechatPublic
                    .map((e) => Tab(
                          text: e.name,
                        ))
                    .toList()),
            Expanded(
                child: TabBarView(
                    children: controller.wechatPublic.map((e) {
              Get.put(WechatPublicChildController(),tag: e.id.toString());
              return WechatPublicChildPage(e.id);
            }).toList()))
          ],
        ));
  }
}
