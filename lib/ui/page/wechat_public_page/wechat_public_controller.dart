import 'package:wan/base/get/base_page_controller.dart';
import 'package:wan/base/get/getx_controller_inject.dart';

import '../../../entity/wechat_public_model.dart';
import 'package:get/get.dart';

import '../wechat_public_child_page/wechat_public_child_controller.dart';

/// @Author rozheng
/// @Date 2024/5/15 11:38
/// @Description TODO
class WeChatPublicController extends BaseGetPageController{
  ///微信公众号列表
  List<WechatPublic> wechatPublic = [];

  RxInt currentIndex = 0.obs; //当前选中的index

  @override
  void onInit() {
    super.onInit();
    getWechatPublic();
  }

  /// 获取公众号作者
  void getWechatPublic() {
    request.getWechatPublic(success: (data) {
      wechatPublic = data;
      update();
    });
  }
}