import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/ui/page/search_page/widget/search_hot_word_item.dart';

import '../../../../base/get/get_common_view.dart';
import '../../../../res/r.dart';
import '../../../../res/string_styles.dart';
import '../../../../res/style.dart';
import '../search_page_controller.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/25 23:15
/// @Description TODO
class SearchHotWordWidget extends GetCommonView<SearchPageController> {
  const SearchHotWordWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Box.hBox20,
            Text(
              StringStyles.searchHotWord.tr,
              style: Styles.style_black_16_bold500,
            ),
            SvgPicture.asset(
              R.assetsImagesHotWord,
              width: 16,
            ),
          ],
        ),
        Box.vBox5,
        Obx(() => Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: GridView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),///设置禁止滚动
                  padding: EdgeInsets.zero,
                  itemCount: controller.hotWord.length,
                  gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2, childAspectRatio: 5 / 1),
                  itemBuilder: (BuildContext content, int index) {
                    return GestureDetector(
                      onTap: () => controller
                          .hotOrHistorySearch(controller.hotWord[index].name),
                      child: SearchHotWordItem(
                        item: controller.hotWord[index],
                        index: index + 1,
                      ),
                    );
                  }),
            ))
      ],
    );
  }
}
