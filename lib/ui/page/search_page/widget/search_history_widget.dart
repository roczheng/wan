import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/ui/page/search_page/search_page_controller.dart';
import 'package:get/get.dart';
import 'package:wan/widget/ripple_widegt.dart';

import '../../../../res/r.dart';
import '../../../../res/string_styles.dart';
import '../../../../res/style.dart';
import 'package:flutter_svg/svg.dart';

import 'search_history_item.dart';

/// @Author rozheng
/// @Date 2024/5/25 22:41
/// @Description
class SearchHistoryWidget extends GetCommonView<SearchPageController> {
  const SearchHistoryWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Obx(() => Visibility(
        visible: controller.history.isNotEmpty,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Box.hBox20,
                Text(
                  StringStyles.searchHistory.tr,
                  style: Styles.style_black_16_bold500,
                ),
                Expanded(child: Container()),
                Ripple(
                    child: Padding(
                  padding: const EdgeInsets.all(5),
                  child: SvgPicture.asset(
                    R.assetsImagesRubbish,
                    width: 24,
                  ),
                )),
                Box.hBox20
              ],
            ),
            Box.vBox5,
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16),
              child: Wrap(
                children: [
                  for (String item in controller.history)
                    SearchHistoryItem(
                        name: item,
                        ///改变输入框内容、设置输入框文本、光标移动到尾部、开始搜索数据
                        onTap: () => controller.hotOrHistorySearch(item))
                ],
              ),
            )
          ],
        )));
  }
}
