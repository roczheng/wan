import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/res/color.dart';
import 'package:wan/res/decoration_style.dart';
import 'package:wan/ui/page/search_page/search_page_controller.dart';
import 'package:wan/widget/ripple_widegt.dart';

import '../../../../res/r.dart';
import '../../../../res/string_styles.dart';
import '../../../../res/style.dart';
import 'package:get/get.dart';
import 'package:flutter_svg/svg.dart';

import '../../../../util/formatter/customized_length_formatter.dart';

/// @Author rozheng
/// @Date 2024/5/25 19:27
/// @Description 顶部搜索框
class SearchTopWidget extends GetCommonView<SearchPageController> {
  ///搜索点击事件
  final GestureTapCallback? onTap;

  ///清空内容点击事件
  final GestureTapCallback? deleteTap;

  ///输入框文字变化
  ValueChanged<String>? onChanged;

  ///输入框控制器
  TextEditingController? textController;

  SearchTopWidget(
      {super.key,
      this.onChanged,
      this.onTap,
      this.deleteTap,
      required this.textController});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Get.theme.primaryColor,
      child: SafeArea(
        top: true,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Box.vBox5,
            Row(
              children: [
                Box.hBox15,
                Ripple(
                  circular: 20,
                  onTap: () => Get.back(),
                  child: const Padding(
                    padding: EdgeInsets.all(5),
                    child: Icon(
                      Icons.arrow_back_ios_new,
                      size: 24,
                      color: Colors.white,
                    ),
                  ),
                ),

                ///水波纹效果
                Box.hBox10,
                Expanded(
                    child: Stack(
                      alignment: AlignmentDirectional.centerStart,
                      children: [
                        Container(
                          height: 36,
                          decoration: DecorationStyle.customize(Colors.white,30),
                          child: TextField(
                            textAlign: TextAlign.left,
                            autofocus: false,
                            maxLines: 1,
                            style: Styles.style_black_14,
                            onChanged: onChanged,
                            controller: textController,
                            inputFormatters: [
                              ///输入长度和格式限制
                              CustomizedLengthTextInputFormatter(20),
                            ],

                            ///样式
                            decoration: InputDecoration(
                                fillColor: Colors.white12,
                                filled: true,
                                hintText: StringStyles.searchHint.tr,
                                hintStyle: Styles.style_B8C0D4_14,
                                border: _getEditBorder(),
                                focusedBorder: _getEditBorder(),
                                disabledBorder: _getEditBorder(),
                                enabledBorder: _getEditBorder(),
                                contentPadding: const EdgeInsets.only(
                                    top: 6, bottom: 6, left: 30, right: 30)),
                          ),
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 5),
                          child: SvgPicture.asset(
                            R.assetsImagesSearch,
                            color: ColorStyle.color_B8C0D4,
                            width: 24,
                          ),
                        ),
                        Positioned(
                            right: 10,
                            child: Obx(() => Visibility(
                                visible: controller.changeText.value.isNotEmpty,
                                child: Ripple(
                                  onTap: deleteTap,
                                  child: const Padding(
                                    padding: EdgeInsets.all(5),
                                    child: Icon(
                                      Icons.cancel_rounded,
                                      color: ColorStyle.color_B8C0D4,
                                      size: 18,
                                    ),
                                  ),
                                ))))
                      ],
                    )),
                Box.hBox5,
                GestureDetector(
                  onTap: onTap,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    child: Text(
                      StringStyles.search.tr,
                      style: Styles.style_white_14,
                    ),
                  ),
                )
              ],
            ),
            Box.vBox10
          ],
        ),
      ),
    );
  }

  ///获取输入框的Border属性，可公用
  ///[isEdit]是否获取焦点
  OutlineInputBorder _getEditBorder() {
    return const OutlineInputBorder(
      borderRadius: BorderRadius.all(Radius.circular(30)),
      borderSide: BorderSide(
        color: Colors.transparent,
      ),
    );
  }
}
