import 'package:flutter/material.dart';
import 'package:wan/ui/page/search_page/widget/search_result_item.dart';

import '../../../../base/get/get_common_view.dart';
import '../../../../util/web_util.dart';
import '../../../../widget/pull_smart_refresher.dart';
import '../../../../widget/ripple_widegt.dart';
import '../search_page_controller.dart';
import 'package:get/get.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
/// @Author rozheng
/// @Date 2024/5/26 17:15
/// @Description TODO
class SearchResultWidget extends GetCommonView<SearchPageController> {
  const SearchResultWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Obx(() => Visibility(
      visible: controller.showResult.value,
      child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 16),
          child: RefreshWidget<SearchPageController>(
              child: ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: controller.searchResult.length,
                itemBuilder: (BuildContext context, int index) {
                  return Material(
                      color: Colors.transparent,
                      child: Ripple(
                          onTap: () =>WebUtil.toWebPage(
                              controller.searchResult[index],
                              onResult: (value){
                                controller.searchResult[index].collect = value;
                              }
                          ),
                          child: SearchResultItem(
                            item: controller.searchResult[index],
                          )));
                },
              ))),
    ));
  }
}