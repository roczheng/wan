import 'package:flutter/material.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/ui/page/search_page/widget/search_result_widget.dart';
import '../../../res/style.dart';
import 'search_page_controller.dart';
import 'widget/search_history_widget.dart';
import 'widget/search_hotword_widget.dart';
import 'widget/search_top_widget.dart';

/// @Author rozheng
/// @Date 2024/5/25 19:01
/// @Description TODO
class SearchPage extends GetCommonView<SearchPageController> {
  const SearchPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomInset:false,//防止软键盘弹出布局溢出，加了这个属性，同时布局需要是可滚动的
      body: Column(
        children: [
          //顶部搜索框
          SearchTopWidget(textController: controller.textController,
            onChanged: (text) {
              controller.changeText.value = text;
              if (text.isEmpty) {
                controller.searchResult.value = [];
              }
            },
            onTap: () => controller.searchWord(),
            deleteTap: () {
              controller
                ..changeText.value = ''
                ..showResult.value = false
                ..textController.text = ''
                ..searchResult.value = [];
            },),

          Box.vBox15,

          Expanded(
            child: Stack(
              children: [
                SingleChildScrollView(
                  child:  Column(
                    children: [
                      ///搜索历史
                      const SearchHistoryWidget(),
                      Box.vBox20,

                      ///搜索热词
                      const SearchHotWordWidget(),
                    ],
                  ),
                ),
                const SearchResultWidget()
              ],
            ),
          )
        ],
      )
    );
  }
}
