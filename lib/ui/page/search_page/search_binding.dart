import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wan/ui/page/search_page/search_page_controller.dart';
/// @Author rozheng
/// @Date 2024/5/25 18:59
/// @Description TODO
class SearchBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => SearchPageController());
  }
}