import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/entity/integral_entity.dart';
import 'package:wan/ui/page/integral_page/integral_controller.dart';

import '../../../entity/project_entity.dart';
import '../../../res/color.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../util/date_util.dart';
import '../../../widget/pull_smart_refresher.dart';
import '../../../widget/toolbar.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/26 23:16
/// @Description 积分明细
class IntegralPage extends GetCommonView<IntegralController> {
  const IntegralPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          ToolBar(
            title: StringStyles.homePoints.tr,
          ),
          DividerStyle.divider1Half,
          Expanded(
              child: RefreshWidget<IntegralController>(
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  itemCount: controller.projectData.length,
                  itemBuilder: (BuildContext context, int index) {
                    IntegralEntity entity = controller.projectData[index];

                    return Stack(
                      children: [
                        Container(
                          margin: const EdgeInsets.symmetric(horizontal: 16,vertical: 10),
                          child:  Row(
                            children: [
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      entity.reason.tr,
                                      style: Styles.style_black_18_bold,
                                    ),
                                    Text(
                                      DateUtil.milliseconds2Date(entity.date),
                                      style: Styles.style_black_16,
                                    ),
                                  ],
                                ),
                              ),
                              Text(
                                "+${entity.coinCount}",
                                style:  const TextStyle(color: ColorStyle.color_24CF5F, fontSize: 24,fontWeight: FontWeight.w800),
                              )
                            ],
                          ),
                        ),
                        Positioned(child: DividerStyle.divider1Half)
                      ],
                    );
                  },
                ),
              ))
        ],
      )
    );
  }
}
