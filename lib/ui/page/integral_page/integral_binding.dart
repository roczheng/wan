import 'package:get/get.dart';
import 'package:wan/ui/page/integral_page/integral_controller.dart';
/// @Author rozheng
/// @Date 2024/5/26 23:17
/// @Description TODO
class IntegralBinding extends Bindings{
  @override
  void dependencies() {
   Get.lazyPut(() => IntegralController());
  }

}