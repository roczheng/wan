import 'package:wan/base/get/getx_controller_inject.dart';

import '../../../entity/language.dart';
import '../../../util/locale_util.dart';
import '../../../util/save/sp_util.dart';
import 'package:get/get.dart';
/// @Author rozheng
/// @Date 2024/5/29 16:10
/// @Description TODO
class LanguageController extends BaseGetController{
  ///应用支持的语言
  List<Language> language = languageList;
  @override
  void onInit() {
    super.onInit();
    ///读取语言存储
    var languageModel = SpUtil.getLanguage();
    if (languageModel == null) {
      language[0].isSelect = true;
    } else {
      ///找到当前选中的语言
      language.forEach((item) {
        if (item.name == languageModel.name) {
          item.isSelect = true;
        }
      });
    }
  }

  void saveLanguage(int index) {
    language
      ..forEach((item) {
        item.isSelect = false;
      })
      ..[index].isSelect = true;
    SpUtil.updateLanguage(language[index]);
    LocaleOptions.updateLocale(language[index]);
    Get.back();
  }
}