import 'package:get/get.dart';
import 'package:wan/ui/page/language_page/language_controller.dart';
/// @Author rozheng
/// @Date 2024/5/29 16:10
/// @Description TODO
class LanguageBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => LanguageController());
  }
}