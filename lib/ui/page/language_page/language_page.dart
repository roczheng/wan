import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/res/string_styles.dart';
import 'package:wan/ui/page/language_page/language_controller.dart';
import 'package:wan/widget/toolbar.dart';

import '../../../res/color.dart';
import '../../../res/style.dart';
import 'package:get/get.dart';

import '../../../widget/over_scroll_behavior.dart';

/// @Author rozheng
/// @Date 2024/5/29 16:10
/// @Description TODO
class LanguagePage extends GetCommonView<LanguageController>{
  const LanguagePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        child: Column(
          children: [
            ToolBar(
              title: StringStyles.settingLanguage.tr,
            ),
            DividerStyle.divider1Half,
            ScrollConfiguration(
                behavior: OverScrollBehavior(),
                child: ListView.builder(
                  padding: EdgeInsets.zero,
                  shrinkWrap: true,
                  itemCount: controller.language.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                        onTap: () {
                          ///更新语言
                         controller.saveLanguage(index);
                        },
                        title: Text(controller.language[index].name),
                        trailing: Visibility(
                          visible: controller.language[index].isSelect,
                          child:  Icon(
                            Icons.radio_button_on,
                            color: Get.theme.primaryColor,
                            size: 20,
                          ),
                        ));
                  },
                ))
          ],
        ),
      ),
    );
  }

}