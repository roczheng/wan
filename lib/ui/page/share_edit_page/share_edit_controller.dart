import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:wan/base/get/base_page_controller.dart';
import 'package:wan/http/http_request.dart';

import '../../../util/toast_utils.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/11 19:31
/// @Description TODO
class ShareEditController extends BaseGetPageController {
  ///输入框文本控制器
  TextEditingController textTitleController = TextEditingController(text: '');

  ///输入框文本控制器
  TextEditingController textLinkController = TextEditingController(text: '');

  ///标题
  RxString title = "".obs;

  ///链接
  RxString link = "".obs;

  ///当前按钮是否可点击
  bool changeShowButton() {
    return title.value.isNotEmpty && link.value.isNotEmpty;
  }

  ///提交分享
  submitShare() {
    if (title.value.isEmpty || link.value.isEmpty) {
      return;
    }
    request.submitShare(title.value, link.value, success: (data) {
      if (kDebugMode) {
        print(data);
      }
      ToastUtils.show("分享成功");
      title.value = '';
      link.value = '';

      textTitleController.text = "";
      textLinkController.text = "";

      //清除掉输入框内容
    }, fail: (code, message) {
      ToastUtils.show(message);
    });
  }
}
