import 'package:get/get.dart';
import 'package:wan/ui/page/share_edit_page/share_edit_controller.dart';

/// @Author rozheng
/// @Date 2024/5/11 19:32
/// @Description TODO
class ShareEditBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ShareEditController());
  }
}
