import 'package:flutter/material.dart';
import 'package:wan/res/color.dart';

import '../../../../res/style.dart';
import '../../../../util/formatter/customized_length_formatter.dart';

/// @Author rozheng
/// @Date 2024/5/13 10:21
/// @Description TODO
class EditShareWidget extends StatefulWidget {
  ///输入框文字改变
  final ValueChanged<String>? onChanged;

  ///提示文字
  String hintText = "";

  ///长度限制
  int length = 0;

  ///高度
  int height = 50;

  TextEditingController? controller;//输入框文本控制器

  EditShareWidget(
      {super.key,
      this.onChanged,
      this.hintText = "",
      this.length = 0,
      this.height = 50,
      this.controller});

  @override
  State<StatefulWidget> createState() {
    return _EditShareState();
  }
}

class _EditShareState extends State<EditShareWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(top: 8, bottom: 8, left: 25, right: 25),
      decoration: const BoxDecoration(
          color: ColorStyle.color_FAFAFB,
          borderRadius: BorderRadius.all(Radius.circular(12))),
      child: TextField(
        controller:widget.controller,
        textInputAction: TextInputAction.done,
        keyboardType: TextInputType.multiline,
        maxLines: 5,
        maxLength: (widget.length > 0) ? widget.length : null,
        // 设置为多行输入，以支持自动换行
        textAlign: TextAlign.left,
        autofocus: false,
        style: Styles.style_black_16,
        onChanged: (text) {
          if (widget.onChanged != null) {
            widget.onChanged!(text);
          }
        },
        inputFormatters: [
          ///输入长度和格式限制
          if (widget.length > 0)
            CustomizedLengthTextInputFormatter(widget.length),
        ],

        ///样式
        decoration: InputDecoration(
            border: InputBorder.none,
            fillColor: Colors.white12,
            filled: true,
            hintText: widget.hintText,
            hintStyle: Styles.style_black_16,
            contentPadding: const EdgeInsets.only(
                top: 16, bottom: 16, left: 16, right: 16)),
      ),
    );
  }
}
