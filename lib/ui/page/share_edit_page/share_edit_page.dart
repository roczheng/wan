import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/res/color.dart';
import 'package:wan/ui/page/share_edit_page/widget/edit_share_widget.dart';

import '../../../res/button_style.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../util/KeyboardUtils.dart';
import '../../../widget/toolbar.dart';
import 'share_edit_controller.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/11 19:30
/// @Description 分享编辑页面
class ShareEditPage extends GetCommonView<ShareEditController> {
  const ShareEditPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,

      ///resizeToAvoidBottomInset: false,//解决软键盘弹出布局溢出的问题
      body: Column(
        children: [
          ToolBar(
            title: StringStyles.homePartake.tr,
          ),
          Expanded(
              child: ListView(
            children: [
              EditShareWidget(
                hintText: "请输入分享标题",
                length: 100,
                height: 300,
                onChanged: (text) => {
                  controller
                    ..title.value = text
                    ..update(),
                },
                controller: controller.textTitleController,
              ),
              EditShareWidget(
                hintText: "请输入分享链接",
                height: 100,
                onChanged: (text) => {
                  controller
                    ..link.value = text
                    ..update(),
                },
                controller: controller.textLinkController,
              ),

              ///提交按钮
              Container(
                width: double.infinity,
                height: 50,
                margin: const EdgeInsets.only(top: 36, left: 25, right: 25),
                decoration: BoxDecoration(
                  color: controller.changeShowButton()
                      ? Colors.blueAccent
                      : Colors.blueGrey,
                  borderRadius: const BorderRadius.all(Radius.circular(30)),
                ),
                child: TextButton(
                    style: controller.changeShowButton()
                        ? ButtonStyles.getButtonStyle()
                        : ButtonStyles.getTransparentStyle(),
                    onPressed: () {
                      KeyboardUtils.hideKeyboard(context);
                      controller.submitShare();
                    },
                    child: Text(
                      StringStyles.enter.tr,
                      style: controller.changeShowButton()
                          ? Styles.style_white_18
                          : Styles.style_white24_18,
                    )),
              ),
            ],
          ))
        ],
      ),
    );
  }
}
