import 'package:flutter/material.dart';
import 'package:wan/base/get/getx_controller_inject.dart';

import '../../../entity/language.dart';
import '../../../routes/routes.dart';
import '../../../util/locale_util.dart';
import '../../../util/save/sp_util.dart';
import 'package:get/get.dart';

import '../../../widget/custom_theme.dart';

/// @Author rozheng
/// @Date 2024/5/11 10:57
/// @Description TODO
class SettingController extends BaseGetController {

  final currentThemeIndex = 0.obs;

  ///应用支持的语言
  List<Language> language = languageList;

  List<String> themeList = [
    "跟随系统",
    "浅色模式",
    "深色模式",
  ];

  ///退出登录
  exitLoginState() {
    SpUtil.deleteUserInfo();
    request.exitLogin();
    Get.offAllNamed(Routes.loginPage);
  }

  String getLanguageTitle() {
    String languageTitle = "";

    ///读取语言存储
    var languageModel = SpUtil.getLanguage();
    if (languageModel == null) {
      language[0].isSelect = true;
      languageTitle = language[0].name;
    } else {
      ///找到当前选中的语言
      language.forEach((item) {
        if (item.name == languageModel.name) {
          item.isSelect = true;
          languageTitle = item.name;
        }
      });
    }
    return languageTitle;
  }

  void saveLanguage(int index) {
    language
      ..forEach((item) {
        item.isSelect = false;
      })
      ..[index].isSelect = true;
    SpUtil.updateLanguage(language[index]);
    LocaleOptions.updateLocale(language[index]);
    Get.back();
  }

  String getThemeTitle() {
    int index = SpUtil.getAppTheme();
    return themeList[index];
  }

  ///切换主题
  void changeTheme(BuildContext context, int themeIndex) {
    ThemeData themeData;
    switch (themeIndex) {
      case 0: //跟隨系統
        themeData = MediaQuery.of(context).platformBrightness == Brightness.dark
            ? darkTheme
            : lightTheme;
        break;
      case 1: //浅色模式
        themeData = lightTheme;
        break;
      case 2: //深色模式
        themeData = darkTheme;
        break;
      default:
        themeData = MediaQuery.of(context).platformBrightness == Brightness.dark
            ? darkTheme
            : lightTheme;
        break;
    }
    //保存到本地
    SpUtil.putAppTheme(themeIndex);
    Get.changeTheme(themeData);
    //使用Get 强制更新app状态
    Future.delayed(const Duration(milliseconds: 300), () {
      print("执行这里");
      Get.forceAppUpdate();
    });
  }
}