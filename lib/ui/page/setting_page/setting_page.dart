import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/base/get/get_extension.dart';
import 'package:wan/res/color.dart';
import 'package:wan/ui/page/setting_page/setting_controller.dart';

import '../../../res/button_style.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import 'package:get/get.dart';

import '../../../routes/routes.dart';
import '../../../util/KeyboardUtils.dart';
import '../../../widget/toolbar.dart';

/// @Author rozheng
/// @Date 2024/5/11 10:56
/// @Description TODO
class SettingPage extends GetCommonView<SettingController> {
  const SettingPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SizedBox(
          height: double.infinity,
          width: double.infinity,
          child: Column(
            children: [
              ToolBar(
                title: StringStyles.settingTitle.tr,
              ),
              Box.vBox20,
              Container(
                margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: ListTile(
                  onTap: () => {
                    Get.toNamed(Routes.themePage)
                    ///_showModalBottomSheet(context)
                  },
                  title: const Align(
                    alignment: Alignment.centerLeft,
                    child: Text("主题"),
                  ),
                  subtitle: Text(controller.getThemeTitle()),
                  trailing: const Icon(
                    Icons.arrow_forward_ios,
                    size: 16,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16), // 边角半径
                    ///side: const BorderSide(color: Colors.grey), // 边框颜色
                  ),
                  tileColor: ColorStyle.color_F3F3F3,
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: ListTile(
                  onTap: () => {
                    ///Get.toNamed(Routes.languagePage)
                    //显示语言选择弹出框
                    changeLanguage(context)
                  },
                  title: const Align(
                    alignment: Alignment.centerLeft,
                    child: Text("语言"),
                  ),
                  subtitle: Text(controller.getLanguageTitle()),
                  trailing: const Icon(
                    Icons.arrow_forward_ios,
                    size: 16,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16), // 边角半径
                    ///side: const BorderSide(color: Colors.grey), // 边框颜色
                  ),
                  tileColor: ColorStyle.color_F3F3F3,
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                child: ListTile(
                  onTap: () => {Get.toNamed(Routes.demoPage)},
                  title: const Align(
                    alignment: Alignment.centerLeft,
                    child: Text("demo"),
                  ),
                  trailing: const Icon(
                    Icons.arrow_forward_ios,
                    size: 16,
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(16), // 边角半径
                    ///side: const BorderSide(color: Colors.grey), // 边框颜色
                  ),
                  tileColor: ColorStyle.color_F3F3F3,
                ),
              ),
              const Expanded(child: SizedBox()),

              ///退出按钮
              Container(
                width: double.infinity,
                height: 50,
                margin: const EdgeInsets.only(top: 36, left: 25, right: 25),
                decoration: BoxDecoration(
                  color: Get.theme.primaryColor,
                  borderRadius: const BorderRadius.all(Radius.circular(30)),
                ),
                child: TextButton(
                    style: ButtonStyles.getButtonStyle(),
                    onPressed: () {
                      Get.showDialog(
                          title: '提示',
                          content: '是否需要退出登陆？',
                          nextTap: () => {controller.exitLoginState()});
                    },
                    child: const Text(
                      "退出登陆",
                      style: Styles.style_white_18,
                    )),
              ),
              Box.vBox20
            ],
          ),
        ));
  }

  Future<void> changeLanguage(BuildContext context) async {
    int? i = await showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
              title: const Text("请选择语言"),
              children: controller.language
                  .asMap()
                  .map((key, value) => MapEntry(
                      key,
                      SimpleDialogOption(
                        onPressed: () {
                          Navigator.pop(context, key);
                        },
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 6),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(child: Text(value.name)),
                              Visibility(
                                visible: controller.language[key].isSelect,
                                child: Icon(
                                  Icons.radio_button_on,
                                  color: Get.theme.primaryColor,
                                  size: 20,
                                ),
                              )
                            ],
                          ),
                        ),
                      )))
                  .values
                  .toList());
        });

    if (i != null) {
      print("选择了：${i}");
      controller.saveLanguage(i);
    }
  }

  /// 弹出底部菜单列表模态对话框选择主题
  Future<int?> _showModalBottomSheet(BuildContext context) {
    return showModalBottomSheet<int>(
      context: context,
      builder: (BuildContext context) {
        return  Column(
          children: [
            Box.vBox20,
            const Text("选择主题",style: Styles.style_black_16_bold,),
            Box.vBox20,
            Expanded(child:  ListView.separated(
                shrinkWrap: true,
                itemBuilder: (context, index) {
                  return Container(
                    height: 50,
                    padding: const EdgeInsets.only(left: 16, right: 16),
                    child: InkWell(
                      onTap: () {
                        controller.changeTheme(context, index);
                        Get.back(result: index);
                      },
                      child: Row(
                        children: [
                          Expanded(child: Text(controller.themeList[index])),
                          Visibility(
                              visible:
                              controller.currentThemeIndex.value == index,
                              child: const Icon(Icons.check_rounded,
                                  color: Colors.blue))
                        ],
                      ),
                    ),
                  );
                },
                separatorBuilder: (context, index) {
                  return const Divider();
                },
                itemCount: controller.themeList.length))
          ],
        );
      },
    );
  }
}
