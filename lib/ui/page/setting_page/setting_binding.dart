import 'package:get/get.dart';
import 'package:wan/ui/page/setting_page/setting_controller.dart';
/// @Author rozheng
/// @Date 2024/5/11 10:58
/// @Description TODO
class SettingBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => SettingController());
  }

}