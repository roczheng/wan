import 'package:get/get.dart';

import '../../../base/get/base_page_controller.dart';
import '../../../base/refresher_extension.dart';
import '../../../entity/collect_entity.dart';
import '../../../entity/project_entity.dart';
import '../../../widget/pull_smart_refresher.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

/// @Author rozheng
/// @Date 2024/5/26 23:05
/// @Description TODO
class CollectController extends BaseGetPageController{
  List<CollectEntity> projectData = [];

  @override
  void requestData(RefreshController controller, {Refresh refresh = Refresh.first}) {
    print("开始请求数据");
    request.getCollectList(page,success: (data, over) {
      RefreshExtension.onSuccess(controller, refresh, over);

      ///下拉刷新需要清除列表
      if (refresh != Refresh.down) {
        projectData.clear();
      }
      projectData.addAll(data);
      showSuccess(projectData);
      update();
    }, fail: (code, msg) {
      showError();
      RefreshExtension.onError(controller, refresh);
    });
  }
}