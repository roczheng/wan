import 'package:flutter/material.dart';
import 'package:wan/entity/project_entity.dart';

import '../../../../entity/collect_entity.dart';
import '../../../../res/color.dart';
import '../../../../res/r.dart';
import '../../../../res/style.dart';
import 'package:flutter_svg/svg.dart';

/// @Author rozheng
/// @Date 2024/5/26 23:13
/// @Description TODO
class CollectListItem extends StatelessWidget {
  CollectEntity detail;

  Function(bool) onResult;

  CollectListItem(this.detail, this.onResult, {super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          padding: const EdgeInsets.all(10),
          decoration: const BoxDecoration(
              color: ColorStyle.color_F9F9F9,
              // 阴影的颜色，模糊半径
              boxShadow: [BoxShadow(color: ColorStyle.color_F9F9F9, blurRadius: 3)],
              borderRadius: BorderRadius.all(Radius.circular(15))),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ///标题
              Text(
                detail.title,
                overflow: TextOverflow.ellipsis,
                maxLines: 2,
                style: Styles.style_black_16_bold,
              ),
              Box.vBox10,

              ///作者
              Row(
                children: [
                  SvgPicture.asset(
                    R.assetsImagesProgram,
                    width: 16,
                  ),
                  Box.hBox10,
                  Text(
                    detail.author,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: Styles.style_6A6969_14,
                  )
                ],
              ),
              Box.vBox10,

              ///发布时间
              Row(
                children: [
                  SvgPicture.asset(
                    R.assetsImagesDateTime,
                    width: 16,
                  ),
                  Box.hBox10,
                  Text(
                    detail.niceDate,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: Styles.style_6A6969_14,
                  )
                ],
              )
            ],
          )),
    );
  }
}