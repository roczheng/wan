import 'package:get/get.dart';

import 'collect_controller.dart';

/// @Author rozheng
/// @Date 2024/5/26 23:05
/// @Description TODO
class CollectBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut<CollectController>(() => CollectController());
  }
}