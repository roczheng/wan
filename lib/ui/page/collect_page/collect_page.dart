import 'package:flutter/material.dart';
import 'package:wan/base/get/get_common_view.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../widget/pull_smart_refresher.dart';

import '../../../widget/toolbar.dart';
import 'collect_controller.dart';
import 'package:get/get.dart';

import 'widget/collect_list_item.dart';

/// @Author rozheng
/// @Date 2024/5/26 23:04
/// @Description TODO
class CollectPage extends GetCommonView<CollectController> {
  const CollectPage({super.key});

  @override
  Widget build(BuildContext context) {
    print("进来了");
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          ToolBar(
            title: StringStyles.homeCollect.tr,
          ),
          DividerStyle.divider1Half,
          Expanded(child:  RefreshWidget<CollectController>(
            child: ListView.builder(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: controller.projectData.length,
              itemBuilder: (BuildContext context, int index) {
                return CollectListItem(
                    controller.projectData[index], (value) {});
              },
            ),
          ))
        ],
      )
    );
  }
}