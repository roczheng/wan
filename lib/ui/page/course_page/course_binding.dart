import 'package:get/get.dart';
import 'package:wan/ui/page/course_page/course_controller.dart';
/// @Author rozheng
/// @Date 2024/5/20 19:23
/// @Description TODO
class CourseBinding extends Bindings{
  @override
  void dependencies() {
   Get.lazyPut(() => CourseController());
  }

}