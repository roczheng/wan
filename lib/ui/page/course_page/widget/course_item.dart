import 'package:flutter/material.dart';
import 'package:wan/entity/project_entity.dart';
import 'package:wan/res/shadow_style.dart';
import 'package:wan/res/style.dart';

import '../../../../entity/course_entity.dart';

/**
 * @Author: rozheng
 * @Date: 2024/5/19 12:24
 * @Description:
 */



class CourseItem extends StatelessWidget {
  CourseEntity courseEntity;

  Function(CourseEntity)? onItemClick;



  CourseItem(this.courseEntity,this.onItemClick,{super.key});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        decoration: ShadowStyle.white12Circle(),
        margin: const EdgeInsets.all(5),
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(5),
              child: Image.network(
                courseEntity.cover,
                fit: BoxFit.fill,
                height: 200,
              ),
            ),
            Box.hBox5,
            Text(
              courseEntity.author,
              style: Styles.style_1A2F51_18,
            ),
            Text(
              courseEntity.desc,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: Styles.style_black_16_bold,
            )
          ],
        ),
      ),
      onTap: ()=>{
        onItemClick?.call(courseEntity)
      },
    );
  }
}
