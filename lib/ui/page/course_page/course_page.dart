import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/ui/page/course_page/course_controller.dart';
import 'package:wan/ui/page/course_page/widget/course_item.dart';
import 'package:wan/widget/toolbar.dart';
import 'package:get/get.dart';

import '../../../routes/routes.dart';
/**
 * @Author: rozheng
 * @Date: 2024/5/19 12:11
 * @Description: 教程首页
 */
class CoursePage extends GetCommonView<CourseController> {
  const CoursePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          ToolBar(
            title: "教程",
          ),
          Expanded(
              child: GridView.builder(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            itemCount: controller.projectData.length,
            itemBuilder: (BuildContext context, int index) {
              return CourseItem(
                  controller.projectData[index], (courseEntity) => {
                    //跳转到教程详情
                   Get.toNamed(Routes.courseChapterPage,arguments: {
                     "id":courseEntity.id
                   })
              });
            },
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2, //横轴三个子widget
                childAspectRatio: 0.6 //宽高比为1时，子widget
                ),
          ))
        ],
      ),
    );
  }
}
