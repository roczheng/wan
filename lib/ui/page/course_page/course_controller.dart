import 'package:wan/base/get/getx_controller_inject.dart';

import '../../../entity/course_entity.dart';
import '../../../entity/project_entity.dart';

/**
 * @Author: rozheng
 * @Date: 2024/5/19 12:13
 * @Description:
 */
class CourseController extends BaseGetController{

  List<CourseEntity> projectData = [];

  void getCourseList() {
    request.getCourseList(success: (data) {
      projectData = data;
      update();
    });
  }

  @override
  void onInit() {
    // TODO: implement onInit
    super.onInit();
    getCourseList();
  }

}