import 'package:get/get.dart';
import 'package:wan/ui/page/clip_page/clip_controller.dart';
import 'package:wan/ui/page/clip_page/clip_page.dart';

/// @Author rozheng
/// @Date 2024/6/5 20:23
/// @Description TODO
class ClipBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ClipController());
  }
}
