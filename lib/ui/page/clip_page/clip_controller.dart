

import 'package:flutter/material.dart';
import 'package:wan/base/get/get_extension.dart';
import 'package:wan/base/get/getx_controller_inject.dart';
import 'package:get/get.dart';

import '../../../entity/user_entity.dart';
import '../../../event/event_bus.dart';
import '../../../res/string_styles.dart';
import '../../../util/file/file.dart';
import '../../../util/save/sp_util.dart';
import '../../../util/toast_utils.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'dart:developer' as developer;
import '../../../event/event.dart';

/// @Author rozheng
/// @Date 2024/6/5 20:22
/// @Description TODO
class ClipController extends BaseGetController {
  ///用户信息
  late UserEntity userInfo;

  @override
  void onInit() {
    super.onInit();
    var info = SpUtil.getUserInfo();
    if (info != null) {
      userInfo = info;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();

  }

  // void saveImage({required ExtendedImageEditorState? state}) async {
  //   print("开始保存图片,显示对话框");
  //   Get.showLoading();
  //   EditImageInfo editImageInfo =
  //       await cropImageDataWithDartLibrary(state: state!!);
  //   final result = await ImageGallerySaver.saveImage(editImageInfo.data!!);
  //   print("result-->$result");
  //   Get.dismiss();
  //   // 处理返回的结果
  //   if (result['isSuccess']) {
  //     String? filePath = result['filePath'];
  //     developer.log('Image saved successfully: $filePath');
  //     ToastUtils.show(StringStyles.saveSuccess.tr);
  //     //更新用户信息
  //     // imagePath=image.path;
  //     userInfo.icon=filePath!!;
  //     SpUtil.putUserInfo(userInfo);
  //     bus.emit(Event.infoUpdate, userInfo);
  //     Get.back();
  //   } else {
  //     developer.log('Failed to save image');
  //     ToastUtils.show("保存失败");
  //   }

    // Uint8List readImageData = await file.readAsBytesSync();
  //}
}
