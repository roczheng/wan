import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/res/style.dart';
import 'package:wan/ui/page/clip_page/clip_controller.dart';
import 'package:wan/widget/toolbar.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/6/5 20:21
/// @Description 裁剪图片
class ClipPage extends GetCommonView<ClipController> {
  const ClipPage({super.key});

  @override
  Widget build(BuildContext context) {
    String imagePath = Get.arguments["path"];

    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              ToolBar(
                title: "图片裁剪",
                rightText: "保存",
                rightOnTap: (){
                  ///controller.saveImage(state: editorKey.currentState);
                },
              ),
            ],
          ),
          // Expanded(
          //     child: ExtendedImage.file(
          //   File(imagePath),
          //   cacheRawData: true,
          //   fit: BoxFit.contain,
          //   mode: ExtendedImageMode.editor,
          //   extendedImageEditorKey: editorKey,
          //   initEditorConfigHandler: (state) {
          //     return EditorConfig(
          //         maxScale: 8.0,
          //         cropRectPadding: EdgeInsets.all(2.0),
          //         hitTestSize: 20.0,
          //         cropAspectRatio: CropAspectRatios.ratio1_1);
          //   },
          // ))
        ],
      ),
    );
  }
}
