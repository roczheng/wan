import '../../../base/get/getx_controller_inject.dart';
import 'package:get/get.dart';

import '../../../entity/user_entity.dart';
import '../../../event/event.dart';
import '../../../event/event_bus.dart';
import '../../../util/permission.dart';
import '../../../util/save/sp_util.dart';

/// @Author rozheng
/// @Date 2024/5/10 17:56
/// @Description TODO
class MeController extends BaseGetController{
  ///用户信息
  late UserEntity userInfo;

  ///浏览历史长度
  RxInt browseHistory = 0.obs;

  @override
  void onInit() {
    super.onInit();
    var info = SpUtil.getUserInfo();
    if (info != null) {
      userInfo = info;
      update();
    }

    bus.on(Event.infoUpdate,onUserInfoChanged);
  }

  ///更新历史记录长度
  void notifyBrowseHistory() {
    browseHistory.value = SpUtil.getBrowseHistoryLength();
  }

  void onUserInfoChanged(e){
     print("收到头像更新啦${e}");
     userInfo=e;
     update();
  }

  @override
  void dispose() {
    bus.off(Event.infoUpdate,onUserInfoChanged);
    super.dispose();
  }
}