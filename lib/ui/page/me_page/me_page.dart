import 'package:flutter/material.dart';
import 'package:wan/base/get/get_save_state_view.dart';
import 'package:wan/ui/page/me_page/me_controller.dart';

import '../../../res/color.dart';
import '../../../res/shadow_style.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../routes/routes.dart';
import '../../../widget/icon_text_widget.dart';
import 'widget/head_circle_widget.dart';
import 'package:get/get.dart';
import 'widget/title_content_widget.dart';

/// @Author rozheng
/// @Date 2024/4/30 17:16
/// @Description TODO
class MePage extends GetSaveView<MeController> {
  const MePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,

        ///margin: const EdgeInsets.only(top: kTextTabBarHeight + 6),
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                      color: Get.theme.primaryColor,
                      borderRadius: const BorderRadius.only(
                          bottomLeft: Radius.circular(40),
                          bottomRight: Radius.circular(40))),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    GestureDetector(
                      onTap: () => {Get.toNamed(Routes.personalInfoPage)},
                      child:
                          ///头像和名字
                          Row(
                        children: [
                          ///头像
                          Container(
                            margin: const EdgeInsets.only(
                                left: 24, top: kTextTabBarHeight + 6),
                            decoration: ShadowStyle.black12Circle40(),
                            child: HeadCircleWidget(
                              width: 72,
                              height: 72,
                              imagePath: controller.userInfo.icon,
                            ),
                          ),

                          ///用户名称
                          Container(
                            margin: const EdgeInsets.only(
                                left: 16, top: kTextTabBarHeight + 6),
                            child: Text(
                              controller.userInfo.nickname,
                              style: Styles.style_white_18,
                            ),
                          ),

                          ///占位
                          const Expanded(
                            child: Text(''), // 中间用Expanded控件
                          ),
                        ],
                      ),
                    ),

                    ///收藏和积分
                    Container(
                      margin:
                          const EdgeInsets.only(top: 30, left: 25, right: 25),
                      decoration: ShadowStyle.white12Circle(),
                      child: Flex(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        direction: Axis.horizontal,
                        children: [
                          ///收藏数量
                          Expanded(
                            flex: 1,
                            child: TitleContentWidget(
                              title: StringStyles.homeCollect.tr,
                              content: controller.userInfo.collectIds.length
                                  .toString(),
                              onTap: () => Get.toNamed(Routes.collectPage),
                            ),
                          ),

                          ///积分
                          Expanded(
                            flex: 1,
                            child: TitleContentWidget(
                              title: StringStyles.homePoints.tr,
                              content: controller.userInfo.coinCount.toString(),
                              onTap: () => Get.toNamed(Routes.integralPage),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),

            ///底部菜单
            Container(
              width: double.infinity,
              alignment: Alignment.centerLeft,
              margin: const EdgeInsets.only(top: 16, left: 25, right: 25),
              decoration: ShadowStyle.white12Circle(),
              child: Column(children: [
                IconTitleWidget(
                  icon: Icons.share,
                  text: "我的分享",
                  endColor: Colors.black54,
                  onTap: () => Get.toNamed(Routes.mySharePage),
                ),
                IconTitleWidget(
                  icon: Icons.history,
                  text: "浏览历史",
                  endColor: Colors.black54,
                  onTap: () => Get.toNamed(Routes.historyPage),
                ),
                IconTitleWidget(
                  icon: Icons.settings,
                  text: "设置",
                  endColor: Colors.black54,
                  onTap: () => Get.toNamed(Routes.settingPage),
                )
              ]),
            )
          ],
        ),
      ),
    );
  }
}
