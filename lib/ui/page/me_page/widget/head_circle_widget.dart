import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// @Author rozheng
/// @Date 2023/4/24 19:41
/// @Description TODO
class HeadCircleWidget extends StatelessWidget{

  double width = 60;
  double height = 60;

  String imagePath="";

  HeadCircleWidget({
    super.key ,
    this.width = 60 ,
    this.height = 60,
    this.imagePath=""
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(50),
      child:Container(
        width: width,
        height: height,
        color: Colors.white,
        child:  loadImageFromSource(imagePath),
      ) ,
    );
  }
}
// 假设的默认图片资源URL
const String defaultImageUrl = 'https://img1.baidu.com/it/u=3580669984,532975628&fm=253&fmt=auto&app=138&f=JPEG?w=800&h=800';

// 根据传入的字符串加载图片
Widget loadImageFromSource(String? imageSource) {
  // 检查是否为空或空字符串
  if (imageSource == null || imageSource.isEmpty) {
    return Image.network(defaultImageUrl,fit: BoxFit.cover,); // 使用默认图片
  }

  // 检查是否为网络资源（这里只是简单检查是否以http或https开头）
  if (imageSource.startsWith('http://') || imageSource.startsWith('https://')) {
    return Image.network(imageSource,fit: BoxFit.cover); // 使用网络图片
  }

  // 尝试作为本地文件加载（这里需要确保文件路径是正确的）
  final File file = File(imageSource);
  if (file.existsSync()) { // 确保文件存在
    return Image.file(file,fit: BoxFit.cover); // 使用本地文件图片
  } else {
    // 如果文件不存在，则使用默认图片或进行错误处理
    return Image.network(defaultImageUrl,fit: BoxFit.cover); // 或者你可以选择显示一个错误图标或占位符
  }
}