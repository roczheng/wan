import 'package:flutter/material.dart';
import 'package:wan/res/shadow_style.dart';

import '../../../../entity/project_entity.dart';
import '../../../../res/style.dart';
import '../../../../util/web_util.dart';

/**
 * @Author: rozheng
 * @Date: 2024/5/19 11:55
 * @Description:
 */
class WechatPublicItem extends StatelessWidget {

  ProjectDetail detail;

  Function(bool)? onResult;

  WechatPublicItem(
      this.detail, {
        super.key,
        this.onResult
      });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => WebUtil.toWebPage(detail , onResult: onResult),
      child: Container(
        width: double.infinity,
        margin: const EdgeInsets.symmetric(horizontal: 10,vertical: 5),
        decoration: ShadowStyle.white12Circle(),
        padding: const EdgeInsets.all(8.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    detail.title,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: Styles.style_black_16_bold,
                  ),
                  Box.vBox10,

                  Text(
                    detail.niceDate,
                    style: Styles.style_B8C0D4_13,
                  )
                ],
              ),
            ),
            Box.hBox5,
          ],
        ),
      ),
    );
  }
}