import 'package:flutter/material.dart';
import 'package:wan/base/get/get_save_state_view.dart';
import 'package:wan/ui/page/wechat_public_child_page/wechat_public_child_controller.dart';

import '../../../res/string_styles.dart';
import '../../../widget/pull_smart_refresher.dart';
import '../../../widget/toolbar.dart';
import '../ask_page/ask_controller.dart';
import 'package:get/get.dart';

import '../ask_page/widget/ask_item_widget.dart';
import 'widget/wechat_public_item.dart';

class WechatPublicChildPage extends StatefulWidget {

  int id;

  WechatPublicChildPage(this.id, {super.key});

  ///Get 局部更新字段
  get updateId => null;

  ///widget生命周期
  get lifecycle => null;

  @override
  AutoDisposeState createState() => AutoDisposeState();
}

///AutomaticKeepAliveClientMixin 保持页面在tab切换的时候不重建，WidgetsBindingObserver屏幕状态监听
class AutoDisposeState extends State<WechatPublicChildPage>
    with
        AutomaticKeepAliveClientMixin<WechatPublicChildPage>,
        WidgetsBindingObserver {
  AutoDisposeState();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<WechatPublicChildController>(
        tag: widget.id.toString(),
        id: widget.updateId,
        builder: (controller) {
          return Scaffold(
            backgroundColor: Colors.white,
            body: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                    child: RefreshWidget<WechatPublicChildController>(
                      tag: widget.id.toString(),
                      child: ListView.builder(
                        padding: EdgeInsets.zero,
                        shrinkWrap: true,

                        ///该属性表示是否根据子组件的总长度来设置ListView的长度，默认值为false
                        ///。默认情况下，ListView会在滚动方向尽可能多的占用空间。
                        ///当ListView在一个无边界(滚动方向上)的容器中时，shrinkWrap必须为true
                        itemCount: controller.projectData.length,
                        itemBuilder: (BuildContext context, int index) {
                          return WechatPublicItem(
                            controller.projectData[index],
                            onResult: (value) {
                              controller.projectData[index].collect = value;
                            },
                          );
                        },
                      ),
                    )),
              ],
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    GetInstance()
        .find<WechatPublicChildController>(tag: widget.id.toString())
        .id = widget.id;
    if (widget.lifecycle != null) {
      WidgetsBinding.instance?.addObserver(this);
    }
  }

  @override
  void dispose() {
    Get.delete<WechatPublicChildController>(tag: widget.id.toString());
    if (widget.lifecycle != null) {
      WidgetsBinding.instance?.removeObserver(this);
    }
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (widget.lifecycle != null) {
      widget.lifecycle(state);
    }
  }

  @override
  bool get wantKeepAlive => true;
}
