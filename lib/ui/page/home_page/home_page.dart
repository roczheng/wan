import 'package:flutter/material.dart';
import 'package:wan/ui/page/home_main_page/home_main_page.dart';
import 'package:wan/ui/page/square_page/square_page.dart';
import 'package:wan/ui/page/wechat_public_page/wechat_public_page.dart';

import '../../../res/color.dart';
import '../../../res/r.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../../widget/custom_tabs.dart' as customTab;
import 'package:get/get.dart';
import 'package:flutter_svg/svg.dart';
import 'package:wan/widget/ripple_widegt.dart';

import '../../../routes/routes.dart';
import '../../../widget/ripple_widegt.dart';
import '../ask_page/ask_page.dart';

/// @Author rozheng
/// @Date 2024/4/30 17:15
/// @Description TODO
class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  TabController? tabController;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: [
            Container(
                color: Get.theme.primaryColor,
                child: SafeArea(
                  top: true,
                  child: Row(
                    children: [
                      Container(
                        alignment: Alignment.topLeft,

                        ///导航栏
                        child: customTab.TabBar(
                          isScrollable: true,
                          //是否可以滑动
                          labelColor: Colors.white,
                          //标签颜色
                          unselectedLabelColor: Colors.white70,
                          controller: tabController,
                          labelStyle: Styles.style_FE8C28_24_bold,
                          unselectedLabelStyle: Styles.style_FFAE2E_16,
                          indicatorColor: Colors.white,
                          tabs: [
                            Tab(
                              text: StringStyles.tabHome.tr,
                            ),
                            const Tab(
                              text: "公众号",
                            )
                          ],
                        ),
                      ),

                      ///间隔
                      const Expanded(child: SizedBox()),

                      ///搜索框
                      Ripple(
                          circular: 20,
                          onTap: () => {Get.toNamed(Routes.searchPage)},
                          child: Padding(
                            padding: const EdgeInsets.all(5),
                            child: SvgPicture.asset(
                              R.assetsImagesSearch,
                              color: Colors.white,
                              width: 30,
                            ),
                          )),
                      Box.hBox20
                    ],
                  ),
                )),
            Expanded(
                child:  TabBarView(
                  controller: tabController,
                  children: const [HomeMainPage(), WeChatPublicPage()],
                )),
          ],
        ));
  }
}
