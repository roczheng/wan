import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/ui/page/register_page/register_controller.dart';

import '../../../base/get/get_common_view.dart';
import '../../../res/button_style.dart';
import '../../../res/color.dart';
import '../../../res/string_styles.dart';
import '../../../res/style.dart';
import '../../../util/KeyboardUtils.dart';
import '../login_page/widget/edit_widget.dart';

import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/8 16:15
/// @Description 注册页面
class RegisterPage extends GetCommonView<RegisterController> {

  const RegisterPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ///账户名输入框
            EditWidget(
              iconWidget: const Icon(
                Icons.perm_identity,
                color: Colors.black,
              ),
              hintText: StringStyles.registerAccountEmpty.tr,
              onChanged: (text) => controller
                ..username = text
                ..update(),
            ),
            ///密码框
            EditWidget(
              iconWidget: const Icon(
                Icons.lock,
                color: Colors.black,
              ),
              hintText: StringStyles.registerPasswordEmpty.tr,
              onChanged: (text) => controller
                ..password = text
                ..update(),
            ),
            ///确认密码框
            EditWidget(
              iconWidget: const Icon(
                Icons.lock,
                color: Colors.black,
              ),
              hintText: StringStyles.registerRePasswordEmpty.tr,
              onChanged: (text) => controller
                ..rePassword = text
                ..update(),
            ),
            ///注册按钮
            Container(
              width: double.infinity,
              height: 50,
              margin: const EdgeInsets.only(top: 36, left: 25, right: 25),
              decoration: BoxDecoration(
                color: controller.changeShowButton()
                    ? ColorStyle.color_24CF5F
                    : ColorStyle.color_24CF5F_20,
                borderRadius: const BorderRadius.all(Radius.circular(30)),
              ),
              child: TextButton(
                  style: controller.changeShowButton()
                      ? ButtonStyles.getButtonStyle()
                      : ButtonStyles.getTransparentStyle(),
                  onPressed: () {
                    KeyboardUtils.hideKeyboard(context);
                    controller.register();
                  },
                  child: Text(
                    StringStyles.enter.tr,
                    style: controller.changeShowButton()
                        ? Styles.style_white_18
                        : Styles.style_white24_18,
                  )),
            ),
          ],
        ),
      ),
    );
  }
}
