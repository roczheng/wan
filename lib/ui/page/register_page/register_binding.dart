
import 'package:get/get.dart';
import 'package:wan/ui/page/register_page/register_controller.dart';
/// @Author rozheng
/// @Date 2024/5/8 16:16
/// @Description TODO
class RegisterBinding extends Bindings{
  @override
  void dependencies() {
    Get.lazyPut(() => RegisterController());
  }
}