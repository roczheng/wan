import 'package:get/get.dart';
import 'package:wan/ui/page/course_child_page/course_child_controller.dart';
/// @Author rozheng
/// @Date 2024/5/22 20:01
/// @Description TODO
class CourseChildBinding extends Bindings{
  @override
  void dependencies() {
   Get.lazyPut(() => CourseChildController());
  }

}