import 'package:wan/base/get/base_page_controller.dart';

import '../../../base/refresher_extension.dart';
import '../../../entity/project_entity.dart';
import '../../../widget/pull_smart_refresher.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/22 20:01
/// @Description TODO
class CourseChildController extends BaseGetPageController {
  List<ProjectDetail> projectData = [];

  @override
  void requestData(RefreshController controller,
      {Refresh refresh = Refresh.first}) {
    int id = Get.arguments["id"];
    print("id is "+id.toString());
    print("page is "+page.toString());

    request.requestCourseChapter(page, id, success: (data, over) {
      RefreshExtension.onSuccess(controller, refresh, over);

      ///下拉刷新需要清除列表
      if (refresh != Refresh.down) {
        projectData.clear();
      }
      projectData.addAll(data);
      showSuccess(projectData);
      update();
    }, fail: (code, msg) {
      showError();
      RefreshExtension.onError(controller, refresh);
    });
  }
}