import 'package:flutter/material.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/res/shadow_style.dart';
import 'package:wan/res/style.dart';
import 'package:wan/ui/page/course_child_page/course_child_controller.dart';
import 'package:wan/widget/toolbar.dart';

import '../../../util/web_util.dart';
import '../../../widget/pull_smart_refresher.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/22 20:01
/// @Description TODO
class CourseChildPage extends GetCommonView<CourseChildController> {
  const CourseChildPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            ToolBar(title: "文章详情",),
            Expanded(child: RefreshWidget<CourseChildController>(
              child: ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                itemCount: controller.projectData.length,
                itemBuilder: (BuildContext context, int index) {
                  String title = "${index + 1} ${controller.projectData[index].title}";
                  return InkWell(
                    child: Column(
                      children: [
                        Container(
                          alignment: Alignment.centerLeft,
                          padding: const EdgeInsets.all(16),
                          child: Text(title,style: Styles.style_1A2F51_18,),
                        ),
                        DividerStyle.divider1Half
                      ],
                    ),
                    onTap: ()=>{
                      WebUtil.toWebPage(
                          controller.projectData[index],
                          onResult: (value) {
                            controller.projectData[index]
                                .collect = value;
                          }),
                    },
                  );
                },
              ),
            ))
          ],
        )
    );
  }
}
