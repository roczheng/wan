import 'package:flutter/material.dart';
import 'package:wan/ui/page/project_child_page/project_child_controller.dart';

import '../../../widget/pull_smart_refresher.dart';
import 'package:get/get.dart';

import '../wechat_public_child_page/widget/wechat_public_item.dart';

/// @Author rozheng
/// @Date 2024/5/23 11:33
/// @Description TODO
class ProjectChildPage extends StatefulWidget {
  int id;

  ProjectChildPage(this.id, {super.key});

  ///Get 局部更新字段
  get updateId => null;

  ///widget生命周期
  get lifecycle => null;

  @override
  AutoDisposeState createState() => AutoDisposeState();
}

///AutomaticKeepAliveClientMixin 保持页面在tab切换的时候不重建，WidgetsBindingObserver屏幕状态监听
class AutoDisposeState extends State<ProjectChildPage>
    with
        AutomaticKeepAliveClientMixin<ProjectChildPage>,
        WidgetsBindingObserver {
  AutoDisposeState();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProjectChildController>(
        tag: widget.id.toString(),
        id: widget.updateId,
        builder: (controller) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Expanded(
                  child: RefreshWidget<ProjectChildController>(
                    tag: widget.id.toString(),
                    child: ListView.builder(
                      padding: EdgeInsets.zero,
                      shrinkWrap: true,

                      ///该属性表示是否根据子组件的总长度来设置ListView的长度，默认值为false
                      ///。默认情况下，ListView会在滚动方向尽可能多的占用空间。
                      ///当ListView在一个无边界(滚动方向上)的容器中时，shrinkWrap必须为true
                      itemCount: controller.projectData.length,
                      itemBuilder: (BuildContext context, int index) {
                        return WechatPublicItem(
                          controller.projectData[index],
                          onResult: (value) {
                            controller.projectData[index].collect = value;
                          },
                        );
                      },
                    ),
                  )),
            ],
          );
        });
  }

  @override
  void initState() {
    super.initState();
    GetInstance().find<ProjectChildController>(tag: widget.id.toString()).id =
        widget.id;
    if (widget.lifecycle != null) {
      WidgetsBinding.instance?.addObserver(this);
    }
  }

  @override
  void dispose() {
    Get.delete<ProjectChildController>(tag: widget.id.toString());
    if (widget.lifecycle != null) {
      WidgetsBinding.instance?.removeObserver(this);
    }
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if (widget.lifecycle != null) {
      widget.lifecycle(state);
    }
  }

  @override
  bool get wantKeepAlive => true;
}
