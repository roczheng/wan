import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:wan/res/color.dart';
import 'package:wan/res/style.dart';
import 'package:wan/ui/page/square_page/square_controller.dart';

import '../../../base/get/get_save_state_view.dart';
import '../../../routes/routes.dart';
import '../../../widget/pull_smart_refresher.dart';
import 'widget/square_article_item.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/4/30 17:17
/// @Description 广场
class SquarePage extends GetSaveView<SquareController> {
  const SquarePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        //SafeArea 用于填充类似于刘海屏、异形屏之类的屏幕的边距, 在其中加入 padding . 保证多端多设备中展示不受影响
        top: true,
        child: RefreshWidget<SquareController>(
          child: ListView.builder(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            itemCount: controller.projectData.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                child: SquareArticleItem(
                  item: controller.projectData[index],
                ),
              );
            },
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Get.toNamed(Routes.shareEditPage);
        },
        shape: const CircleBorder(),
        backgroundColor: ColorStyle.color_FAFAFB,
        child: const Icon(Icons.send),
      ),
    );
  }
}
