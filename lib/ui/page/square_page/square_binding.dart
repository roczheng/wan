import 'package:get/get.dart';
import 'package:wan/ui/page/square_page/square_controller.dart';

/// @Author rozheng
/// @Date 2024/5/11 16:53
/// @Description TODO
class SquareBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SquareController());
  }
}
