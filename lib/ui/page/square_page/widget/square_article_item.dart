import 'package:flutter/material.dart';
import '../../../../../res/style.dart';
import '../../../../entity/project_entity.dart';
import '../../../../res/decoration_style.dart';
import '../../../../util/web_util.dart';

/// @Author rozheng
/// @Date 2024/5/7 22:18
/// @Description TODO
class SquareArticleItem extends StatelessWidget {
  ///列表数据
  ProjectDetail item;

  Function(bool)? onResult;

  SquareArticleItem({super.key, required this.item, this.onResult});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => WebUtil.toWebPage(item, onResult: onResult),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Box.vBox10,
            Text(
              item.title,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: Styles.style_black_16_bold,
            ),
            Box.vBox10,
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Visibility(
                    visible: item.fresh,
                    child: Container(
                      margin: const EdgeInsets.only(top: 4, right: 12),
                      padding: const EdgeInsets.symmetric(
                          horizontal: 3, vertical: 2),
                      decoration: DecorationStyle.customize(Colors.red, 3),
                      child: const Text(
                        '新',
                        style: Styles.style_white_10,
                      ),
                    )),
                Text(
                  item.shareUser.isEmpty ? item.author : item.shareUser,
                  style: Styles.style_9F9EA6_11,
                ),
                Box.hBox10,
                Text(item.niceDate, style: Styles.style_9F9EA6_11),
              ],
            ),
            Box.vBox10,
            DividerStyle.divider1Half
          ],
        ),
      ),
    );
  }
}
