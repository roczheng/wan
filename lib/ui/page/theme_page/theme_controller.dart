import 'package:flutter/material.dart';
import 'package:wan/base/get/getx_controller_inject.dart';
import 'package:wan/entity/language.dart';
import 'package:wan/util/save/sp_util.dart';

import '../../../util/locale_util.dart';
import '../../../widget/custom_theme.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/28 15:10
/// @Description TODO
class ThemeController extends BaseGetController {
  final currentThemeIndex = 0.obs;

  final sliderValue = 0.0.obs;

  final hue = 0.0.obs; // 假设我们仅处理色调（Hue）作为示例

  List<String> themeList = [
    "跟随系统",
    "浅色模式",
    "深色模式",
  ];

  @override
  void onInit() {
    super.onInit();
    currentThemeIndex.value = SpUtil.getAppTheme();
  }

  ///切换主题
  void changeTheme(BuildContext context, int themeIndex) {
    ThemeData themeData;
    switch (themeIndex) {
      case 0: //跟隨系統
        themeData = MediaQuery.of(context).platformBrightness == Brightness.dark
            ? darkTheme
            : lightTheme;
        break;
      case 1: //浅色模式
        themeData = lightTheme;
        break;
      case 2: //深色模式
        themeData = darkTheme;
        break;
      default:
        themeData = MediaQuery.of(context).platformBrightness == Brightness.dark
            ? darkTheme
            : lightTheme;
        break;
    }
    //保存到本地
    SpUtil.putAppTheme(themeIndex);
    Get.changeTheme(themeData);
    //使用Get 强制更新app状态
    Future.delayed(const Duration(milliseconds: 300), () {
      print("执行这里");
      Get.forceAppUpdate();
    });
  }
}
