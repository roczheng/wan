import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/ui/page/theme_page/theme_controller.dart';
import 'package:wan/widget/toolbar.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/28 15:10
/// @Description 主题切换
class ThemePage extends GetCommonView<ThemeController> {
  const ThemePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          ToolBar(
            title: "主题",
          ),
          // 滑块
          // Slider(
          //   value: controller.sliderValue.value,
          //   min: 0.0,
          //   max: 1.0,
          //   onChanged: (double value) {
          //     controller.sliderValue.value = value;
          //     controller.update();
          //   },
          // ),
          // // 颜色预览
          // Container(
          //   margin: EdgeInsets.all(16),
          //   height: 100,
          //   color: _getColorFromValue(controller.sliderValue.value),
          // ),
          GestureDetector(
            onHorizontalDragUpdate: (details) {
              // 计算新的色调值（这里只是一个示例，你需要根据UI和滑动事件来调整）
              controller.hue.value = (details.localPosition.dx / 300).clamp(0.0, 1.0); // 假设滑块宽度为300
              controller.update();
            },
            child: CustomPaint(
              painter: _ColorSliderPainter(controller.hue.value),
              size: Size(300, 50), // 假设滑块尺寸为300x50
            ),
          ),
          Expanded(
            child: ListView.separated(
                itemBuilder: (context, index) {
                  return Obx(() => Container(
                        padding: const EdgeInsets.only(left: 16, right: 16),
                        height: 50,
                        child: InkWell(
                          onTap: () {
                            controller.changeTheme(context, index);
                            Get.back(result: index);
                          },
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text(controller.themeList[index])),
                              Visibility(
                                  visible: controller.currentThemeIndex.value ==
                                      index,
                                  child: const Icon(Icons.check_rounded,
                                      color: Colors.blue))
                            ],
                          ),
                        ),
                      ));
                },
                separatorBuilder: (context, index) {
                  return const Divider();
                },
                itemCount: controller.themeList.length),
          )
        ],
      ),
    );
  }
}

Color _getColorFromValue(double value) {
  // 根据滑块的值生成颜色，这里使用线性插值
  int red = (255 - (value * 255)).round();
  int blue = (value * 255).round();
  return Color.fromARGB(255, red, 0, blue); // 红色渐变到蓝色
}

class _ColorSliderPainter extends CustomPainter {
  final double hue;

  _ColorSliderPainter(this.hue);

 @override
  void paint(Canvas canvas, Size size) {
   ///final Paint paint = Paint()..isAntiAlias = true;

   print("hue===>${hue}");

   // 绘制颜色滑块背景（这里仅为灰色作为示例）
   // paint.color = Colors.grey[200]!;
   //
   // canvas.drawRect(Rect.fromLTWH(0, 0, size.width, size.height), paint);

   // 创建一个从透明到红色的颜色列表
   final List<Color> colors = [
     Colors.blue,
     Colors.red,
   ];

   // 创建一个LinearGradient对象，这里假设我们想要垂直渐变
   final LinearGradient gradient = LinearGradient(
     colors: colors,
     begin: Alignment.centerLeft, // 渐变开始的位置
     end: Alignment.centerRight, // 渐变结束的位置
     tileMode: TileMode.clamp, // 当渐变不足以填充整个画布时如何处理
   );

   // 创建一个Paint对象，并将渐变设置为其Shader
   final Paint paint = Paint()
     ..shader = gradient.createShader(Rect.fromLTWH(0.0, 0.0, size.width, size.height));

   // 使用Paint对象绘制整个画布
   canvas.drawRect(Rect.fromLTWH(0.0, 0.0, size.width, size.height), paint);

   // 绘制当前选择的颜色（基于色调值）
   final Color color = HSVColor.fromAHSV(1.0, hue, 1.0, 1.0).toColor(); // 假设有一个HSVColor类来转换HSV到Color
   print("color${color.value}");
   paint.color = _getColorFromValue(hue);
   canvas.drawRect(Rect.fromLTWH(size.width * hue - 10, 0, 20, size.height), paint); // 绘制一个小的矩形来表示当前颜色
  }

  // 注意：HSVColor类在Flutter的标准库中不存在，你需要自己实现它或使用第三方库。
  // 这里的代码只是一个示例，用于说明如何在Canvas上绘制基于色调的颜色。

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return oldDelegate != this;
  }
}


