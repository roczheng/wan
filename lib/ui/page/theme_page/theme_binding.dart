import 'package:get/get.dart';
import 'package:wan/ui/page/theme_page/theme_controller.dart';
/// @Author rozheng
/// @Date 2024/5/28 15:11
/// @Description TODO
class ThemeBinding extends Bindings{
  @override
  void dependencies() {
     Get.lazyPut(() => ThemeController());
  }
}