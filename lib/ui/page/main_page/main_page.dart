import 'package:flutter/material.dart';
import 'package:wan/res/color.dart';
import 'package:wan/res/shadow_style.dart';
import 'package:get/get.dart';
import 'package:wan/ui/page/home_page/home_page.dart';
import 'package:wan/ui/page/me_page/me_page.dart';
import 'package:wan/ui/page/square_page/square_page.dart';
import 'package:wan/util/toast_utils.dart';

import '../../../res/string_styles.dart';
import 'widget/main_tab_title.dart';

/// @Author rozheng
/// @Date 2024/4/30 16:07
/// @Description 首页
class MainPage extends StatefulWidget {
  const MainPage({super.key});

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  TabController? tabController;

  @override
  void initState() {
    // TODO: implement initState
    tabController = TabController(length: 3, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    DateTime? lastPressedAt; //上次点击时间

    return WillPopScope(
        onWillPop: () async {
          if (lastPressedAt == null ||
              DateTime.now().difference(lastPressedAt!) >
                  const Duration(seconds: 1)) {
            //两次点击间隔超过1秒则重新计时
            lastPressedAt = DateTime.now();
            ToastUtils.show("再按一次退出");
            return false;
          }
          return true;
        },
        child: Scaffold(
          backgroundColor: ColorStyle.color_F8F9FC,
          body: TabBarView(
            controller: tabController,
            children: const [HomePage(), SquarePage(), MePage()],
          ),
          bottomNavigationBar: Container(
            height: 65,
            decoration: ShadowStyle.white12TopSpread4Blur10(radius: 0),
            child: Container(
              color: Get.theme.primaryColor,
              child: TabBar(
                indicator: const BoxDecoration(),
                labelColor: Colors.white,
                unselectedLabelColor: Colors.white70,
                controller: tabController,
                tabs: [
                  TabTitleIcon(
                    title: StringStyles.tabHome.tr,
                    icon: Icons.home,
                  ),
                  TabTitleIcon(
                    title: StringStyles.tabSquare.tr,
                    icon: Icons.sports_volleyball_sharp,
                  ),
                  TabTitleIcon(
                    title: StringStyles.homeMy.tr,
                    icon: Icons.person,
                  )
                ],
              ),
            ),
          ),
        ));
  }
}
