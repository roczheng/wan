import 'package:get/get.dart';
import 'package:wan/ui/page/home_main_page/home_main_controller.dart';
import 'package:wan/ui/page/me_page/me_controller.dart';
import 'package:wan/ui/page/square_page/square_controller.dart';
import 'package:wan/ui/page/wechat_public_child_page/wechat_public_child_page.dart';
import 'package:wan/ui/page/wechat_public_page/wechat_public_controller.dart';

import '../wechat_public_child_page/wechat_public_child_controller.dart';

/// @Author rozheng
/// @Date 2024/5/11 11:29
/// @Description TODO
class MainBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => SquareController());
    Get.lazyPut(() => MeController());
    Get.lazyPut(() => HomeMainController());
    Get.lazyPut(() => WeChatPublicController());
  }
}
