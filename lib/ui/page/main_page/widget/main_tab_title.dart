import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

/// @Author rozheng
/// @Date 2024/4/30 16:27
/// @Description TODO
class TabTitleIcon extends StatelessWidget {
  String title = "";
  IconData? icon;

  TabTitleIcon({
    Key? key,
    required this.title,
    this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Tab(
      text: title,
      iconMargin: const EdgeInsets.all(4),
      icon: Icon(
        icon,
      ),
    );
  }
}
