import 'package:get/get.dart';
import 'package:wan/ui/page/project_page/project_controller.dart';

/// @Author rozheng
/// @Date 2024/5/23 10:57
/// @Description TODO
class ProjectBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ProjectController());
  }
}
