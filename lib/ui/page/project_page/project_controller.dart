import 'package:wan/base/get/base_page_controller.dart';
import 'package:wan/entity/project_classify_entity.dart';
import '../../../base/refresher_extension.dart';
import '../../../entity/project_entity.dart';
import '../../../widget/pull_smart_refresher.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/23 10:57
/// @Description TODO
class ProjectController extends BaseGetPageController {
  List<ProjectClassifyEntity> projectClassify = [];

  RxInt currentIndex = 0.obs; //当前选中的index

  @override
  void onInit() {
    super.onInit();
    getWechatPublic();
  }

  /// 获取公众号作者
  void getWechatPublic() {
    request.getProjectClassify(success: (data) {
      projectClassify = data;
      update();
    });
  }
}
