import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:wan/base/get/get_common_view.dart';
import 'package:wan/ui/page/project_child_page/project_child_controller.dart';
import 'package:wan/ui/page/project_child_page/project_child_page.dart';
import 'package:wan/ui/page/project_page/project_controller.dart';
import 'package:wan/widget/toolbar.dart';

import '../../../res/color.dart';
import '../../../res/style.dart';
import 'package:get/get.dart';

/// @Author rozheng
/// @Date 2024/5/23 10:56
/// @Description 项目
class ProjectPage extends GetCommonView<ProjectController>{
  const ProjectPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: DefaultTabController(
          length: controller.projectClassify.length,
          child: Column(
            children: [
              ToolBar(title: "项目",),
              TabBar(
                  tabAlignment: TabAlignment.start,
                  isScrollable: true,
                  //是否可以滑动
                  labelColor: Get.theme.primaryColor,
                  //标签颜色
                  unselectedLabelColor: Colors.black38,

                  labelStyle: Styles.style_black_16_bold,
                  unselectedLabelStyle: Styles.style_FFAE2E_16,

                  indicatorColor:Get.theme.primaryColor,
                  tabs: controller.projectClassify
                      .map((e) => Tab(
                    text: e.name,
                  ))
                      .toList()),
              Expanded(
                  child: TabBarView(
                      children: controller.projectClassify.map((e) {
                        Get.put(ProjectChildController(),tag: e.id.toString());
                        return ProjectChildPage(e.id);
                      }).toList()))
            ],
          )),
    );
  }
}