import 'package:wan/generated/json/base/json_field.dart';
import 'package:wan/generated/json/course_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class CourseEntity {
	late List<dynamic> articleList;
	late String author;
	late List<dynamic> children;
	late int courseId;
	late String cover;
	late String desc;
	late int id;
	late String lisense;
	late String lisenseLink;
	late String name;
	late int order;
	late int parentChapterId;
	late int type;
	late bool userControlSetTop;
	late int visible;

	CourseEntity();

	factory CourseEntity.fromJson(Map<String, dynamic> json) => $CourseEntityFromJson(json);

	Map<String, dynamic> toJson() => $CourseEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}