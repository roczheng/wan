import 'dart:convert';

class Classify {
  late List<ClassifyModuleDataArticles> articles;
  late int cid;
  late String name;

  Classify();

  @override
  String toString() {
    return jsonEncode(this);
  }

  Classify.fromJson(Map<dynamic, dynamic> json) {
    cid = json["cid"];
    name = json["name"];
    dynamic jsonChild = json["articles"];
    List<dynamic> list = jsonChild.map((value) {
      return ClassifyModuleDataArticles.fromJson(value);
    }).toList();
    articles = list.cast<ClassifyModuleDataArticles>();
  }
}

class ClassifyModuleDataArticles {
  late String title;
  late String link;
  late int id;
  late int type;//1标题0内容

  ClassifyModuleDataArticles();

  ClassifyModuleDataArticles.fromJson(Map<dynamic, dynamic> json) {

    id = json["id"];
    link = json["link"];
    title = json["title"];
  }

  @override
  String toString() {
    return jsonEncode(this);
  }
}
