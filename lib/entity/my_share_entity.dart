import 'package:wan/generated/json/base/json_field.dart';
import 'package:wan/generated/json/my_share_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class MyShareEntity {
	late MyShareEntityCoinInfo coinInfo;
	late MyShareEntityShareArticles shareArticles;

	MyShareEntity();

	factory MyShareEntity.fromJson(Map<String, dynamic> json) => $MyShareEntityFromJson(json);

	Map<String, dynamic> toJson() => $MyShareEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class MyShareEntityCoinInfo {
	late int coinCount;
	late int level;
	late String nickname;
	late String rank;
	late int userId;
	late String username;

	MyShareEntityCoinInfo();

	factory MyShareEntityCoinInfo.fromJson(Map<String, dynamic> json) => $MyShareEntityCoinInfoFromJson(json);

	Map<String, dynamic> toJson() => $MyShareEntityCoinInfoToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class MyShareEntityShareArticles {
	late int curPage;
	late List<MyShareEntityShareArticlesDatas> datas;
	late int offset;
	late bool over;
	late int pageCount;
	late int size;
	late int total;

	MyShareEntityShareArticles();

	factory MyShareEntityShareArticles.fromJson(Map<String, dynamic> json) => $MyShareEntityShareArticlesFromJson(json);

	Map<String, dynamic> toJson() => $MyShareEntityShareArticlesToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}

@JsonSerializable()
class MyShareEntityShareArticlesDatas {
	late bool adminAdd;
	late String apkLink;
	late int audit;
	late String author;
	late bool canEdit;
	late int chapterId;
	late String chapterName;
	late bool collect;
	late int courseId;
	late String desc;
	late String descMd;
	late String envelopePic;
	late bool fresh;
	late String host;
	late int id;
	late bool isAdminAdd;
	late String link;
	late String niceDate;
	late String niceShareDate;
	late String origin;
	late String prefix;
	late String projectLink;
	late int publishTime;
	late int realSuperChapterId;
	late int selfVisible;
	late int shareDate;
	late String shareUser;
	late int superChapterId;
	late String superChapterName;
	late List<dynamic> tags;
	late String title;
	late int type;
	late int userId;
	late int visible;
	late int zan;

	MyShareEntityShareArticlesDatas();

	factory MyShareEntityShareArticlesDatas.fromJson(Map<String, dynamic> json) => $MyShareEntityShareArticlesDatasFromJson(json);

	Map<String, dynamic> toJson() => $MyShareEntityShareArticlesDatasToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}