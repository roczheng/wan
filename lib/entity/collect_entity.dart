import 'package:wan/generated/json/base/json_field.dart';
import 'package:wan/generated/json/collect_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class CollectEntity {
	late String author;
	late int chapterId;
	late String chapterName;
	late int courseId;
	late String desc;
	late String envelopePic;
	late int id;
	late String link;
	late String niceDate;
	late String origin;
	late int originId;
	late int publishTime;
	late String title;
	late int userId;
	late int visible;
	late int zan;

	CollectEntity();

	factory CollectEntity.fromJson(Map<String, dynamic> json) => $CollectEntityFromJson(json);

	Map<String, dynamic> toJson() => $CollectEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}