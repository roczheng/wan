import 'package:flutter/material.dart';

/// @Author rozheng
/// @Date 2023/7/9 12:29
/// @Description TODO
class MenuMode{

  /// 标题
  String title = '';
  ///图标
  IconData icon;
  ////类型
  int type = 0;

  MenuMode({this.title = '', this.icon=Icons.cabin, this.type = 0});
}