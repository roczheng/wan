import 'package:wan/generated/json/base/json_field.dart';
import 'package:wan/generated/json/integral_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class IntegralEntity {
	late int coinCount;
	late int date;
	late String desc;
	late int id;
	late String reason;
	late int type;
	late int userId;
	late String userName;

	IntegralEntity();

	factory IntegralEntity.fromJson(Map<String, dynamic> json) => $IntegralEntityFromJson(json);

	Map<String, dynamic> toJson() => $IntegralEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}