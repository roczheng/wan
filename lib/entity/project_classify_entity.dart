import 'package:wan/generated/json/base/json_field.dart';
import 'package:wan/generated/json/project_classify_entity.g.dart';
import 'dart:convert';

@JsonSerializable()
class ProjectClassifyEntity {
	late List<dynamic> articleList;
	late String author;
	late List<dynamic> children;
	late int courseId;
	late String cover;
	late String desc;
	late int id;
	late String lisense;
	late String lisenseLink;
	late String name;
	late int order;
	late int parentChapterId;
	late int type;
	late bool userControlSetTop;
	late int visible;

	ProjectClassifyEntity();

	factory ProjectClassifyEntity.fromJson(Map<String, dynamic> json) => $ProjectClassifyEntityFromJson(json);

	Map<String, dynamic> toJson() => $ProjectClassifyEntityToJson(this);

	@override
	String toString() {
		return jsonEncode(this);
	}
}