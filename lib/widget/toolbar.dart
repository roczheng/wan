import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:wan/res/style.dart';

import '../res/color.dart';

/// @Author rozheng
/// @Date 2023/4/16 21:35
/// @Description TODO
class ToolBar extends StatefulWidget {
  ///标题文字
  String title;

  ///背景颜色
  Color? backgroundColor = Get.theme.primaryColor;

  ///返回箭头颜色
  Color? backColor = Colors.white;

  VoidCallback? backOnTap; //返回回调
  VoidCallback? rightOnTap; //左边按钮回调

  ///左边按钮文字
  String rightText;

  ToolBar(
      {super.key,
      this.title = "",
      this.backgroundColor,
      this.backOnTap,
      this.backColor,
      this.rightText = "",
      this.rightOnTap});

  @override
  State<ToolBar> createState() => _ToolBarState();
}

class _ToolBarState extends State<ToolBar> {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: Get.theme.primaryColor,
        child: SafeArea(
          //状态栏适配
          top: true,
          child: SizedBox(
            height: 50,
            width: double.infinity,
            child: Stack(
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: InkWell(
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20),
                    ),
                    onTap: widget.backOnTap ?? () => Get.back(),
                    splashColor: ColorStyle.color_E2E3E8_66,
                    highlightColor: ColorStyle.color_E2E3E8_66,
                    child: Container(
                      margin: const EdgeInsets.all(5),
                      child: Icon(
                        Icons.arrow_back,
                        color: widget.backColor,
                      ),
                    ),
                  ),
                ),
                Align(
                  alignment: Alignment.center,
                  child: Text(
                    widget.title,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: const TextStyle(color: Colors.white, fontSize: 16),
                  ),
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Visibility(
                    visible: widget.rightText.isNotEmpty,
                    child: TextButton(
                      onPressed: widget.rightOnTap,
                      child: Text(
                        widget.rightText,
                        style: Styles.style_black_16_bold,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ));
  }
}
