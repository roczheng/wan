import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../res/color.dart';

/// @Author rozheng
/// @Date 2023/5/3 20:21
/// @Description 点击波纹效果Widget
class Ripple extends StatelessWidget{

  ///点击事件
  GestureTapCallback? onTap;
  ///圆角大小
  double circular;
  ///widget
  Widget child;

  Ripple({
    super.key ,
    this.onTap,
    this.circular = 0,
    required this.child
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(//用InkWell或者GestureDetector将某个组件包起来，可添加点击事件。GestureDetector 使用点击无水波纹出现，InkWell可以实现水波纹效果
      borderRadius: BorderRadius.all(
        Radius.circular(circular),
      ),
      onTap: onTap,
      splashColor: ColorStyle.color_E2E3E8_66,
      highlightColor: ColorStyle.color_E2E3E8_66,
      child: child,
    );
  }


}