import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
abstract class GetCommonView<T extends GetxController> extends StatefulWidget {

  const GetCommonView({ super.key });

  final String? tag = null;//为了区分，统一类不同对象

  T get controller => GetInstance().find<T>(tag: tag);

  ///Get 局部更新字段
  get updateId => null;

  @protected
  Widget build(BuildContext context);

  @override
  AutoDisposeState createState() => AutoDisposeState<T>();
}

class AutoDisposeState<S extends GetxController> extends State<GetCommonView> {
  AutoDisposeState();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<S>(
        id: widget.updateId,
        builder: (controller) {
          return widget.build(context);
        });
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    Get.delete<S>();
    super.dispose();
  }
}
