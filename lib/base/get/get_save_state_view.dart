import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
/// @Author rozheng
/// @Date 2023/4/20 20:29
/// @Description TODO
abstract class GetSaveView<T extends GetxController> extends StatefulWidget {

  const GetSaveView({ super.key });

  final String? tag = null;

  T get controller => GetInstance().find<T>(tag: tag);

  ///Get 局部更新字段
  get updateId => null;

  ///widget生命周期
  get lifecycle => null;

  @protected
  Widget build(BuildContext context);

  @override
  AutoDisposeState createState() => AutoDisposeState<T>();
}

///AutomaticKeepAliveClientMixin 保持页面在tab切换的时候不重建，WidgetsBindingObserver屏幕状态监听
class AutoDisposeState<S extends GetxController> extends State<GetSaveView>
    with AutomaticKeepAliveClientMixin<GetSaveView> ,WidgetsBindingObserver {


  AutoDisposeState();

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return GetBuilder<S>(
        id: widget.updateId,
        builder: (controller) {
          return widget.build(context);
        });
  }

  @override
  void initState() {
    super.initState();
    if(widget.lifecycle != null){
      WidgetsBinding.instance?.addObserver(this);
    }
  }

  @override
  void dispose() {
    Get.delete<S>();
    if(widget.lifecycle != null) {
      WidgetsBinding.instance?.removeObserver(this);
    }
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    if(widget.lifecycle != null) {
      widget.lifecycle(state);
    }
  }

  @override
  bool get wantKeepAlive => true;
}
