import 'package:get/get.dart';
import 'package:wan/ui/page/ask_page/ask_page.dart';
import 'package:wan/ui/page/classify_page/classify_binding.dart';
import 'package:wan/ui/page/classify_page/classify_page.dart';
import 'package:wan/ui/page/clip_page/clip_binding.dart';
import 'package:wan/ui/page/clip_page/clip_page.dart';
import 'package:wan/ui/page/collect_page/collect_binding.dart';
import 'package:wan/ui/page/collect_page/collect_page.dart';
import 'package:wan/ui/page/course_child_page/course_child_binding.dart';
import 'package:wan/ui/page/course_child_page/course_child_controller.dart';
import 'package:wan/ui/page/course_child_page/course_child_page.dart';
import 'package:wan/ui/page/course_page/course_binding.dart';
import 'package:wan/ui/page/course_page/course_page.dart';
import 'package:wan/ui/page/demo_module/demo_page.dart';
import 'package:wan/ui/page/history_page/history_binding.dart';
import 'package:wan/ui/page/history_page/history_page.dart';
import 'package:wan/ui/page/home_page/home_page.dart';
import 'package:wan/ui/page/integral_page/integral_binding.dart';
import 'package:wan/ui/page/integral_page/integral_page.dart';
import 'package:wan/ui/page/language_page/language_binding.dart';
import 'package:wan/ui/page/language_page/language_page.dart';
import 'package:wan/ui/page/main_page/main_binding.dart';
import 'package:wan/ui/page/main_page/main_page.dart';
import 'package:wan/ui/page/my_share_page/my_share_binding.dart';
import 'package:wan/ui/page/my_share_page/my_share_page.dart';
import 'package:wan/ui/page/personal_info_page/personal_info_binding.dart';
import 'package:wan/ui/page/personal_info_page/personal_info_page.dart';
import 'package:wan/ui/page/project_child_page/project_child_page.dart';
import 'package:wan/ui/page/project_page/project_binding.dart';
import 'package:wan/ui/page/project_page/project_controller.dart';
import 'package:wan/ui/page/register_page/register_binding.dart';
import 'package:wan/ui/page/register_page/register_page.dart';
import 'package:wan/ui/page/search_page/search_binding.dart';
import 'package:wan/ui/page/search_page/search_page.dart';
import 'package:wan/ui/page/setting_page/setting_binding.dart';
import 'package:wan/ui/page/setting_page/setting_controller.dart';
import 'package:wan/ui/page/setting_page/setting_page.dart';
import 'package:wan/ui/page/share_edit_page/share_edit_binding.dart';
import 'package:wan/ui/page/share_edit_page/share_edit_page.dart';
import 'package:wan/ui/page/theme_page/theme_binding.dart';
import 'package:wan/ui/page/theme_page/theme_page.dart';

import '../ui/page/ask_page/ask_binding.dart';
import '../ui/page/login_page/login_binding.dart';
import '../ui/page/login_page/login_page.dart';
import '../ui/page/project_page/project_page.dart';
import '../ui/page/splash_page/splash_binding.dart';
import '../ui/page/splash_page/splash_page.dart';
import '../ui/page/webview_page/webview_binding.dart';
import '../ui/page/webview_page/webview_page.dart';
import '../ui/page/wechat_public_child_page/wechat_public_child_page.dart';

abstract class Routes {
  ///入口模块
  static const String splashPage = '/splash';

  ///登录模块
  static const String loginPage = '/login';

  ///注册模块
  static const String registerPage = '/register';

  ///首页模块
  static const String mainPage = "/main";

  ///设置模块
  static const String settingPage = "/setting";

  ///分享内容编辑页面
  static const String shareEditPage = "/share/edit";

  ///我的分享页面
  static const String mySharePage = "/my/share/page";

  ///问答
  static const String askPage = "/ask";

  ///导航分类
  static const String classifyPage = "/classify";

  ///公众号对应作者文章列表
  //static const String wechatPublicItemPage = "/wechat/public/child";

  ///教程分类
  static const String coursePage = "/course";

  ///教程章节
  static const String courseChapterPage = "/course/chapter";

  ///项目
  static const String projectPage = "/course/project";

  ///webView
  static const String webViewPage = '/webView';

  ///搜索页面
  static const String searchPage = '/searchPage';

  ///个人积分列表
  static const String integralPage = '/integralPage';

  ///我的收藏列表
  static const String collectPage = '/collectPage';

  ///历史记录
  static const String historyPage = '/historyPage';

  ///主题
  static const String themePage = '/themePage';

  ///语言
  static const String languagePage = '/languagePage';

  ///个人信息
  static const String personalInfoPage = "/personalInfoPage";

  ///demo
  static const String demoPage = '/demoPage';

  ///裁剪页面
  static const String clipPage ="/clipPage";

  static final routePage = [
    GetPage(
        name: splashPage,
        page: () => const SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: mainPage, page: () => const MainPage(), binding: MainBinding()),
    GetPage(
        name: loginPage,
        page: () => const LoginPage(),
        binding: LoginBinding()),
    GetPage(
        name: registerPage,
        page: () => const RegisterPage(),
        binding: RegisterBinding()),
    GetPage(
        name: settingPage,
        page: () => SettingPage(),
        binding: SettingBinding()),
    GetPage(
        name: shareEditPage,
        page: () => const ShareEditPage(),
        binding: ShareEditBinding()),
    GetPage(
        name: mySharePage,
        page: () => const MySharePage(),
        binding: MyShareBinding()),
    GetPage(name: askPage, page: () => const AskPage(), binding: AskBinding()),
    GetPage(
        name: classifyPage,
        page: () => const ClassifyPage(),
        binding: ClassifyBinding()),
    GetPage(
        name: coursePage,
        page: () => const CoursePage(),
        binding: CourseBinding()),
    GetPage(
        name: courseChapterPage,
        page: () => const CourseChildPage(),
        binding: CourseChildBinding()),
    GetPage(
        name: projectPage,
        page: () => const ProjectPage(),
        binding: ProjectBinding()),
    GetPage(
        name: webViewPage,
        page: () => const WebViewPage(),
        binding: WebViewBinding()),
    GetPage(
        name: searchPage,
        page: () => const SearchPage(),
        binding: SearchBinding()),
    GetPage(
        name: integralPage,
        page: () => const IntegralPage(),
        binding: IntegralBinding()),
    GetPage(
        name: collectPage,
        page: () => const CollectPage(),
        binding: CollectBinding()),
    GetPage(
        name: historyPage,
        page: () => const HistoryPage(),
        binding: HistoryBinding()),
    GetPage(
        name: themePage,
        page: () => const ThemePage(),
        binding: ThemeBinding()),
    GetPage(
        name: languagePage,
        page: () => const LanguagePage(),
        binding: LanguageBinding()),
    GetPage(
        name: personalInfoPage,
        page: () => const PersonalInfoPage(),
        binding: PersonalInfoBinding()),
    GetPage(
        name: clipPage,
        page: () => const ClipPage(),
        binding: ClipBinding()),
    GetPage(name: demoPage, page: () => const DemoPage())
  ];
}
