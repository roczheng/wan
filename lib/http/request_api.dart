/// @Author rozheng
/// @Date 2023/4/17 20:44
/// @Description TODO
class RequestApi {
  ///前缀地址
  static const String baseurl = 'https://www.wanandroid.com/';

  ///登录接口
  static const String apiLogin = 'user/login';

  ///退出登录
  static const String apiLogout = 'user/logout/json';

  ///项目接口
  static const String apiProject = 'article/listproject/page/json';

  ///问答列表
  static const String apiAsk = 'wenda/list/page/json';

  ///广场列表
  static const String apiSquare = 'user_article/list/page/json';

  ///首页文章
  static const String apiHome = 'article/list/page/json';

  ///公众号列表
  static const String apiWechatPublic = 'wxarticle/chapters/json';

  ///首页banner轮播图
  static const String apiBanner = 'banner/json';

  ///取消收藏站内文章
  static const String apiUnCollect = 'lg/uncollect_originId/id/json';

  ///收藏站内文章
  static const String apiCollect = 'lg/collect/id/json';

  ///搜索热词
  static const String apiHotWord = 'hotkey/json';

  ///搜索文章
  static const String apiSearchWord = 'article/query/page/json';

  ///分类导航
  static const String apiClassify = 'navi/json';

  ///收藏列表
  static const String apiCollectList = "lg/collect/list/page/json";

  ///我的分享
  static const String apiMyShare = "user/lg/private_articles/page/json";

  ///分享文章
  static const String apiShareArticle = "lg/user_article/add/json";

  ///删除分享
  static const String apiDeleteMyShare = "lg/user_article/delete/id/json";

  ///公众号历史记录
  static const String apiWechatRecord = "wxarticle/list/id/page/json";

  ///获取教程
  static const String apiCourseList = "chapter/547/sublist/json";

  ///获取教程章节
  static const String apiCourseChapter = 'article/list/page/json';

  ///项目分类
  static const String apiProjectClassify = '/project/tree/json';

  ///项目分类下列表
  static const String apiProjectList = '/project/list/page/json';

  ///获取个人积分列表
  static const String apiIntegralList = '/lg/coin/list/page/json';

}
