import 'package:wan/entity/course_entity.dart';
import 'package:wan/entity/integral_entity.dart';
import 'package:wan/http/request.dart';
import 'package:wan/http/request_api.dart';

import '../entity/banners_model.dart';
import '../entity/classify_model.dart';
import '../entity/collect_entity.dart';
import '../entity/hotword_model.dart';
import '../entity/my_share_entity.dart';
import '../entity/project_classify_entity.dart';
import '../entity/project_entity.dart';
import '../entity/user_entity.dart';
import '../entity/wechat_public_model.dart';
import '../util/save/sp_util.dart';
import 'http_request.dart';

typedef SuccessOver<T> = Function(T data, bool over);

/// @Author rozheng
/// @Date 2023/4/17 20:40
/// @Description TODO
class RequestRepository {
  ///登录请求
  /// [account]账号
  /// [password]密码
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  login(
    String account,
    String password, {
    Success<UserEntity>? success,
    Fail? fail,
  }) {
    Request.post<dynamic>(RequestApi.apiLogin,
        parameters: {"username": account, "password": password},
        success: (data) {
      var loginInfo = UserEntity.fromJson(data);
      loginInfo.password = password;
      SpUtil.putUserInfo(loginInfo);
      if (success != null) {
        success(loginInfo);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///退出登录
  exitLogin({
    Success<bool>? success,
    Fail? fail,
  }) {
    Request.post<dynamic>(RequestApi.apiLogout, dialog: false, success: (data) {
      if (success != null) {
        success(true);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///请求广场列表接口
  /// [page] 当前页面
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  requestSquareModule(
    int page, {
    SuccessOver<List<ProjectDetail>>? success,
    Fail? fail,
  }) {
    Request.get<dynamic>(
        RequestApi.apiSquare.replaceFirst(RegExp('page'), '${page - 1}'),
        dialog: false, success: (data) {
      ProjectPage pageData = ProjectPage.fromJson(data);
      var list = pageData.datas.map((value) {
        return ProjectDetail.fromJson(value);
      }).toList();
      if (success != null) {
        success(list, pageData.over);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///分享文章
  /// [account]账号
  /// [password]密码
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  submitShare(
    String title,
    String link, {
    Success<String>? success,
    Fail? fail,
  }) {
    Request.post<dynamic>(RequestApi.apiShareArticle,
        parameters: {"title": title, "link": link}, success: (data) {
      if (success != null) {
        success("分享成功");
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///请求我的分享列表接口
  /// [page] 当前页面
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  requestMyShareModule(
    int page, {
    SuccessOver<List<MyShareEntityShareArticlesDatas>>? success,
    Fail? fail,
  }) {
    Request.get<dynamic>(
        RequestApi.apiMyShare.replaceFirst(RegExp('page'), '${page - 1}'),
        dialog: false, success: (data) {
      MyShareEntity pageData = MyShareEntity.fromJson(data);
      if (success != null) {
        success(pageData.shareArticles.datas, true);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///删除分享文章
  deleteShare(
    int id, {
    Success<bool>? success,
    Fail? fail,
  }) {
    Request.post<dynamic>(
        RequestApi.apiDeleteMyShare.replaceFirst(RegExp('id'), '${id}'),
        dialog: false, success: (data) {
      if (success != null) {
        success(true);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///请求问答接口
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  requestAskModule(
    int page, {
    SuccessOver<List<ProjectDetail>>? success,
    Fail? fail,
  }) {
    Request.get<dynamic>(
        RequestApi.apiAsk.replaceFirst(RegExp('page'), '$page'),
        dialog: false, success: (data) {
      ProjectPage pageData = ProjectPage.fromJson(data);
      var list = pageData.datas.map((value) {
        return ProjectDetail.fromJson(value);
      }).toList();
      if (success != null) {
        success(list, pageData.over);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///获取首页的Banner图片
  getBanner({
    Success<List<Banners>>? success,
    Fail? fail,
  }) {
    Request.get<List<dynamic>>(RequestApi.apiBanner, dialog: false,
        success: (data) {
      if (success != null) {
        var list = data.map((value) {
          return Banners.fromJson(value);
        }).toList();
        success(list);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///请求首页文章列表接口
  ///[id]文章ID
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  requestHomeArticle(
    int page, {
    SuccessOver<List<ProjectDetail>>? success,
    Fail? fail,
  }) {
    Request.get<dynamic>(
        RequestApi.apiHome.replaceFirst(RegExp('page'), '${page - 1}'),
        dialog: false, success: (data) {
      ProjectPage pageData = ProjectPage.fromJson(data);
      var list = pageData.datas.map((value) {
        return ProjectDetail.fromJson(value);
      }).toList();
      if (success != null) {
        success(list, pageData.over);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///获取导航数据
  getClassify({
    Success<List<Classify>>? success,
    Fail? fail,
  }) {
    Request.get<List<dynamic>>(RequestApi.apiClassify, dialog: false,
        success: (data) {
      if (success != null) {
        var list = data.map((value) {
          return Classify.fromJson(value);
        }).toList();
        success(list);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///获取微信公众号列表
  getWechatPublic({
    Success<List<WechatPublic>>? success,
    Fail? fail,
  }) {
    Request.get<List<dynamic>>(RequestApi.apiWechatPublic, dialog: false,
        success: (data) {
      if (success != null) {
        var list = data.map((value) {
          return WechatPublic.fromJson(value);
        }).toList();
        success(list);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///请求对应公众号历史记录
  ///[id] 对应公众号Id
  /// [page] 当前页面
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  requestWechatRecord(
    int id,
    int page, {
    SuccessOver<List<ProjectDetail>>? success,
    Fail? fail,
  }) {
    Request.get<dynamic>(
        RequestApi.apiWechatRecord
            .replaceFirst(RegExp('page'), '${page - 1}')
            .replaceFirst(RegExp('id'), '${id}'),
        dialog: false, success: (data) {
      ProjectPage pageData = ProjectPage.fromJson(data);
      var list = pageData.datas.map((value) {
        return ProjectDetail.fromJson(value);
      }).toList();
      if (success != null) {
        success(list, pageData.over);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///获取教程
  getCourseList({
    Success<List<CourseEntity>>? success,
    Fail? fail,
  }) {
    Request.get<List<dynamic>>(RequestApi.apiCourseList, dialog: false,
        success: (data) {
      if (success != null) {
        var list = data.map((value) {
          return CourseEntity.fromJson(value);
        }).toList();
        success(list);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///请求教程下的章节列表
  /// [page] 当前页面
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  requestCourseChapter(
    int page,
    int cid, {
    SuccessOver<List<ProjectDetail>>? success,
    Fail? fail,
  }) {
    Request.get<dynamic>(
        RequestApi.apiCourseChapter.replaceFirst(RegExp('page'), '${page - 1}'),
        queryParameters: {"cid": cid, "order_type": 1},
        dialog: false, success: (data) {
      ProjectPage pageData = ProjectPage.fromJson(data);
      var list = pageData.datas.map((value) {
        return ProjectDetail.fromJson(value);
      }).toList();
      if (success != null) {
        success(list, pageData.over);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///请求项目list
  /// [page] 当前页面
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  requestProject(
    int page, {
    SuccessOver<List<ProjectDetail>>? success,
    Fail? fail,
  }) {
    Request.get<dynamic>(
        RequestApi.apiCourseChapter.replaceFirst(RegExp('page'), '${page - 1}'),
        dialog: false, success: (data) {
      ProjectPage pageData = ProjectPage.fromJson(data);
      var list = pageData.datas.map((value) {
        return ProjectDetail.fromJson(value);
      }).toList();
      if (success != null) {
        success(list, pageData.over);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///项目分类
  getProjectClassify({
    Success<List<ProjectClassifyEntity>>? success,
    Fail? fail,
  }) {
    Request.get<List<dynamic>>(RequestApi.apiProjectClassify, dialog: false,
        success: (data) {
      if (success != null) {
        var list = data.map((value) {
          return ProjectClassifyEntity.fromJson(value);
        }).toList();
        success(list);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///项目分类下列表
  requestProjectList(
    int id,
    int page, {
    SuccessOver<List<ProjectDetail>>? success,
    Fail? fail,
  }) {
    Request.get<dynamic>(
        RequestApi.apiProjectList.replaceFirst(RegExp('page'), '${page - 1}'),
        queryParameters: {"cid": id},
        dialog: false, success: (data) {
      ProjectPage pageData = ProjectPage.fromJson(data);
      var list = pageData.datas.map((value) {
        return ProjectDetail.fromJson(value);
      }).toList();
      if (success != null) {
        success(list, pageData.over);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///收藏|取消收藏 文章接口
  ///[id]文章ID
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  collectArticle(
    int id, {
    bool isCollect = false,
    Success<bool>? success,
    Fail? fail,
  }) {
    var api = isCollect ? RequestApi.apiUnCollect : RequestApi.apiCollect;
    Request.post<dynamic>(api.replaceFirst(RegExp('id'), '$id'), dialog: false,
        success: (data) {
      if (success != null) {
        success(true);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///根据关键词搜索文章
  /// [page]当前页码
  /// [hotWord] 当前热词
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  searchKeyWord(
    int page,
    String hotWord, {
    SuccessOver<List<ProjectDetail>>? success,
    Fail? fail,
  }) {
    Request.post<Map<String, dynamic>>(
        RequestApi.apiSearchWord.replaceFirst(RegExp('page'), '$page'),
        parameters: {
          "k": hotWord,
        },
        dialog: false, success: (data) {
      ProjectPage pageData = ProjectPage.fromJson(data);
      var list = pageData.datas.map((value) {
        return ProjectDetail.fromJson(value);
      }).toList();
      if (success != null) {
        success(list, pageData.over);
      }
    }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }

  ///获取搜索热词
  getSearchHotWord({
    Success<List<HotWord>>? success,
    Fail? fail,
  }) {
    Request.get<List<dynamic>>(RequestApi.apiHotWord, dialog: false,
        success: (data) {
          if (success != null) {
            var list = data.map((value) {
              return HotWord.fromJson(value);
            }).toList();
            success(list);
          }
        }, fail: (code, msg) {
          if (fail != null) {
            fail(code, msg);
          }
        });
  }

  ///获取收藏列表
  getCollectList(int page,
      {SuccessOver<List<CollectEntity>>? success, Fail? fail}) {
    print("开始请求收藏列表");
    Request.get<dynamic>(
        RequestApi.apiCollectList.replaceFirst(RegExp('page'), '$page'),dialog: false,
        success: (data) {
          ProjectPage pageData = ProjectPage.fromJson(data);
          var list = pageData.datas.map((value) {
            return CollectEntity.fromJson(value);
          }).toList();
          if (success != null) {
            success(list, pageData.over);
          }
        }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }
  ///获取个人积分列表
  getIntegralList(int page,
      {SuccessOver<List<IntegralEntity>>? success, Fail? fail}) {
    Request.get<dynamic>(
        RequestApi.apiIntegralList.replaceFirst(RegExp('page'), '$page'),dialog: false,
        success: (data) {
          ProjectPage pageData = ProjectPage.fromJson(data);
          var list = pageData.datas.map((value) {
            return IntegralEntity.fromJson(value);
          }).toList();
          if (success != null) {
            success(list, pageData.over);
          }
        }, fail: (code, msg) {
      if (fail != null) {
        fail(code, msg);
      }
    });
  }
}
