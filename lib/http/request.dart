import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:wan/http/get_extension.dart';
import 'package:wan/http/request_api.dart';

import '../entity/request_result.dart';
import '../util/toast_utils.dart';
import 'http_request.dart';

/// @Author rozheng
/// @Date 2023/4/17 20:51
/// @Description TODO
class Request {
  /// 发起GET请求
  /// [url] 请求url
  /// [parameters] 请求参数
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  static void get<T>(
    String url, {
    Map<String, dynamic>? queryParameters,
    bool isJson = false,
    bool dialog = true,
    Success<T>? success,
    Fail? fail,
  }) {
    _request(Method.GET, url,
        queryParameters: queryParameters,
        isJson: isJson,
        dialog: dialog,
        success: success,
        fail: fail);
  }

  /// 发起POST请求
  /// [url] 请求url
  /// [parameters] 请求参数
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  static void post<T>(
    String url, {
        parameters,
    bool isJson = false,
    bool dialog = true,
    Success<T>? success,
    Fail? fail,
  }) {
    _request(Method.POST, url,
        data: parameters,
        isJson: isJson,
        dialog: dialog,
        success: success,
        fail: fail);
  }

  /// 发起请求
  /// [method] 请求类型
  /// [url] 请求url
  /// [parameters] 请求参数
  /// [success] 请求成功回调
  /// [fail] 请求失败回调
  static void _request<T>(
    Method method,
    String url, {
    bool isJson = false,
    bool dialog = true,
    Success<T>? success,
    Fail? fail,
    data,
    Map<String, dynamic>? queryParameters,
  }) {
    if (dialog) {
      Get.showLoading();
    }
    debugPrint("request url ==============> ${RequestApi.baseurl}$url");

    // ///校验参数中是否携带URL
    // data.forEach((key, value) {
    //   if (url.contains(key)) {
    //     url = url.replaceAll(':$key', value.toString());
    //   }
    // });
    //debugPrint("request url new ==============> ${RequestApi.baseurl}$url");

    ///开启请求
    HttpRequest.request(method, url,
        data: data,
        queryParameters: queryParameters,
        isJson: isJson, success: (result) {
      if (dialog) {
        Get.dismiss();
      }
      if (success != null) {
        var resultModel = Result.fromJson(result);
        debugPrint("request success =>$resultModel");
        if (resultModel.errorCode == 0) {
          success(resultModel.data);
        } else {
          ///其他状态，弹出错误提示信息
          ToastUtils.show(resultModel.errorMsg);
        }
      }
    }, fail: (code, msg) {
      debugPrint("request error =>$msg");
      if (dialog) {
        Get.dismiss();
      }
      ToastUtils.show(msg);
      if (fail != null) {
        fail(code, msg);
      }
    });
  }
}
