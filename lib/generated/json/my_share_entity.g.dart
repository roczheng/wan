import 'package:wan/generated/json/base/json_convert_content.dart';
import 'package:wan/entity/my_share_entity.dart';

MyShareEntity $MyShareEntityFromJson(Map<String, dynamic> json) {
	final MyShareEntity myShareEntity = MyShareEntity();
	final MyShareEntityCoinInfo? coinInfo = jsonConvert.convert<MyShareEntityCoinInfo>(json['coinInfo']);
	if (coinInfo != null) {
		myShareEntity.coinInfo = coinInfo;
	}
	final MyShareEntityShareArticles? shareArticles = jsonConvert.convert<MyShareEntityShareArticles>(json['shareArticles']);
	if (shareArticles != null) {
		myShareEntity.shareArticles = shareArticles;
	}
	return myShareEntity;
}

Map<String, dynamic> $MyShareEntityToJson(MyShareEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['coinInfo'] = entity.coinInfo.toJson();
	data['shareArticles'] = entity.shareArticles.toJson();
	return data;
}

MyShareEntityCoinInfo $MyShareEntityCoinInfoFromJson(Map<String, dynamic> json) {
	final MyShareEntityCoinInfo myShareEntityCoinInfo = MyShareEntityCoinInfo();
	final int? coinCount = jsonConvert.convert<int>(json['coinCount']);
	if (coinCount != null) {
		myShareEntityCoinInfo.coinCount = coinCount;
	}
	final int? level = jsonConvert.convert<int>(json['level']);
	if (level != null) {
		myShareEntityCoinInfo.level = level;
	}
	final String? nickname = jsonConvert.convert<String>(json['nickname']);
	if (nickname != null) {
		myShareEntityCoinInfo.nickname = nickname;
	}
	final String? rank = jsonConvert.convert<String>(json['rank']);
	if (rank != null) {
		myShareEntityCoinInfo.rank = rank;
	}
	final int? userId = jsonConvert.convert<int>(json['userId']);
	if (userId != null) {
		myShareEntityCoinInfo.userId = userId;
	}
	final String? username = jsonConvert.convert<String>(json['username']);
	if (username != null) {
		myShareEntityCoinInfo.username = username;
	}
	return myShareEntityCoinInfo;
}

Map<String, dynamic> $MyShareEntityCoinInfoToJson(MyShareEntityCoinInfo entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['coinCount'] = entity.coinCount;
	data['level'] = entity.level;
	data['nickname'] = entity.nickname;
	data['rank'] = entity.rank;
	data['userId'] = entity.userId;
	data['username'] = entity.username;
	return data;
}

MyShareEntityShareArticles $MyShareEntityShareArticlesFromJson(Map<String, dynamic> json) {
	final MyShareEntityShareArticles myShareEntityShareArticles = MyShareEntityShareArticles();
	final int? curPage = jsonConvert.convert<int>(json['curPage']);
	if (curPage != null) {
		myShareEntityShareArticles.curPage = curPage;
	}
	final List<MyShareEntityShareArticlesDatas>? datas = jsonConvert.convertListNotNull<MyShareEntityShareArticlesDatas>(json['datas']);
	if (datas != null) {
		myShareEntityShareArticles.datas = datas;
	}
	final int? offset = jsonConvert.convert<int>(json['offset']);
	if (offset != null) {
		myShareEntityShareArticles.offset = offset;
	}
	final bool? over = jsonConvert.convert<bool>(json['over']);
	if (over != null) {
		myShareEntityShareArticles.over = over;
	}
	final int? pageCount = jsonConvert.convert<int>(json['pageCount']);
	if (pageCount != null) {
		myShareEntityShareArticles.pageCount = pageCount;
	}
	final int? size = jsonConvert.convert<int>(json['size']);
	if (size != null) {
		myShareEntityShareArticles.size = size;
	}
	final int? total = jsonConvert.convert<int>(json['total']);
	if (total != null) {
		myShareEntityShareArticles.total = total;
	}
	return myShareEntityShareArticles;
}

Map<String, dynamic> $MyShareEntityShareArticlesToJson(MyShareEntityShareArticles entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['curPage'] = entity.curPage;
	data['datas'] =  entity.datas.map((v) => v.toJson()).toList();
	data['offset'] = entity.offset;
	data['over'] = entity.over;
	data['pageCount'] = entity.pageCount;
	data['size'] = entity.size;
	data['total'] = entity.total;
	return data;
}

MyShareEntityShareArticlesDatas $MyShareEntityShareArticlesDatasFromJson(Map<String, dynamic> json) {
	final MyShareEntityShareArticlesDatas myShareEntityShareArticlesDatas = MyShareEntityShareArticlesDatas();
	final bool? adminAdd = jsonConvert.convert<bool>(json['adminAdd']);
	if (adminAdd != null) {
		myShareEntityShareArticlesDatas.adminAdd = adminAdd;
	}
	final String? apkLink = jsonConvert.convert<String>(json['apkLink']);
	if (apkLink != null) {
		myShareEntityShareArticlesDatas.apkLink = apkLink;
	}
	final int? audit = jsonConvert.convert<int>(json['audit']);
	if (audit != null) {
		myShareEntityShareArticlesDatas.audit = audit;
	}
	final String? author = jsonConvert.convert<String>(json['author']);
	if (author != null) {
		myShareEntityShareArticlesDatas.author = author;
	}
	final bool? canEdit = jsonConvert.convert<bool>(json['canEdit']);
	if (canEdit != null) {
		myShareEntityShareArticlesDatas.canEdit = canEdit;
	}
	final int? chapterId = jsonConvert.convert<int>(json['chapterId']);
	if (chapterId != null) {
		myShareEntityShareArticlesDatas.chapterId = chapterId;
	}
	final String? chapterName = jsonConvert.convert<String>(json['chapterName']);
	if (chapterName != null) {
		myShareEntityShareArticlesDatas.chapterName = chapterName;
	}
	final bool? collect = jsonConvert.convert<bool>(json['collect']);
	if (collect != null) {
		myShareEntityShareArticlesDatas.collect = collect;
	}
	final int? courseId = jsonConvert.convert<int>(json['courseId']);
	if (courseId != null) {
		myShareEntityShareArticlesDatas.courseId = courseId;
	}
	final String? desc = jsonConvert.convert<String>(json['desc']);
	if (desc != null) {
		myShareEntityShareArticlesDatas.desc = desc;
	}
	final String? descMd = jsonConvert.convert<String>(json['descMd']);
	if (descMd != null) {
		myShareEntityShareArticlesDatas.descMd = descMd;
	}
	final String? envelopePic = jsonConvert.convert<String>(json['envelopePic']);
	if (envelopePic != null) {
		myShareEntityShareArticlesDatas.envelopePic = envelopePic;
	}
	final bool? fresh = jsonConvert.convert<bool>(json['fresh']);
	if (fresh != null) {
		myShareEntityShareArticlesDatas.fresh = fresh;
	}
	final String? host = jsonConvert.convert<String>(json['host']);
	if (host != null) {
		myShareEntityShareArticlesDatas.host = host;
	}
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		myShareEntityShareArticlesDatas.id = id;
	}
	final bool? isAdminAdd = jsonConvert.convert<bool>(json['isAdminAdd']);
	if (isAdminAdd != null) {
		myShareEntityShareArticlesDatas.isAdminAdd = isAdminAdd;
	}
	final String? link = jsonConvert.convert<String>(json['link']);
	if (link != null) {
		myShareEntityShareArticlesDatas.link = link;
	}
	final String? niceDate = jsonConvert.convert<String>(json['niceDate']);
	if (niceDate != null) {
		myShareEntityShareArticlesDatas.niceDate = niceDate;
	}
	final String? niceShareDate = jsonConvert.convert<String>(json['niceShareDate']);
	if (niceShareDate != null) {
		myShareEntityShareArticlesDatas.niceShareDate = niceShareDate;
	}
	final String? origin = jsonConvert.convert<String>(json['origin']);
	if (origin != null) {
		myShareEntityShareArticlesDatas.origin = origin;
	}
	final String? prefix = jsonConvert.convert<String>(json['prefix']);
	if (prefix != null) {
		myShareEntityShareArticlesDatas.prefix = prefix;
	}
	final String? projectLink = jsonConvert.convert<String>(json['projectLink']);
	if (projectLink != null) {
		myShareEntityShareArticlesDatas.projectLink = projectLink;
	}
	final int? publishTime = jsonConvert.convert<int>(json['publishTime']);
	if (publishTime != null) {
		myShareEntityShareArticlesDatas.publishTime = publishTime;
	}
	final int? realSuperChapterId = jsonConvert.convert<int>(json['realSuperChapterId']);
	if (realSuperChapterId != null) {
		myShareEntityShareArticlesDatas.realSuperChapterId = realSuperChapterId;
	}
	final int? selfVisible = jsonConvert.convert<int>(json['selfVisible']);
	if (selfVisible != null) {
		myShareEntityShareArticlesDatas.selfVisible = selfVisible;
	}
	final int? shareDate = jsonConvert.convert<int>(json['shareDate']);
	if (shareDate != null) {
		myShareEntityShareArticlesDatas.shareDate = shareDate;
	}
	final String? shareUser = jsonConvert.convert<String>(json['shareUser']);
	if (shareUser != null) {
		myShareEntityShareArticlesDatas.shareUser = shareUser;
	}
	final int? superChapterId = jsonConvert.convert<int>(json['superChapterId']);
	if (superChapterId != null) {
		myShareEntityShareArticlesDatas.superChapterId = superChapterId;
	}
	final String? superChapterName = jsonConvert.convert<String>(json['superChapterName']);
	if (superChapterName != null) {
		myShareEntityShareArticlesDatas.superChapterName = superChapterName;
	}
	final List<dynamic>? tags = jsonConvert.convertListNotNull<dynamic>(json['tags']);
	if (tags != null) {
		myShareEntityShareArticlesDatas.tags = tags;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		myShareEntityShareArticlesDatas.title = title;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		myShareEntityShareArticlesDatas.type = type;
	}
	final int? userId = jsonConvert.convert<int>(json['userId']);
	if (userId != null) {
		myShareEntityShareArticlesDatas.userId = userId;
	}
	final int? visible = jsonConvert.convert<int>(json['visible']);
	if (visible != null) {
		myShareEntityShareArticlesDatas.visible = visible;
	}
	final int? zan = jsonConvert.convert<int>(json['zan']);
	if (zan != null) {
		myShareEntityShareArticlesDatas.zan = zan;
	}
	return myShareEntityShareArticlesDatas;
}

Map<String, dynamic> $MyShareEntityShareArticlesDatasToJson(MyShareEntityShareArticlesDatas entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['adminAdd'] = entity.adminAdd;
	data['apkLink'] = entity.apkLink;
	data['audit'] = entity.audit;
	data['author'] = entity.author;
	data['canEdit'] = entity.canEdit;
	data['chapterId'] = entity.chapterId;
	data['chapterName'] = entity.chapterName;
	data['collect'] = entity.collect;
	data['courseId'] = entity.courseId;
	data['desc'] = entity.desc;
	data['descMd'] = entity.descMd;
	data['envelopePic'] = entity.envelopePic;
	data['fresh'] = entity.fresh;
	data['host'] = entity.host;
	data['id'] = entity.id;
	data['isAdminAdd'] = entity.isAdminAdd;
	data['link'] = entity.link;
	data['niceDate'] = entity.niceDate;
	data['niceShareDate'] = entity.niceShareDate;
	data['origin'] = entity.origin;
	data['prefix'] = entity.prefix;
	data['projectLink'] = entity.projectLink;
	data['publishTime'] = entity.publishTime;
	data['realSuperChapterId'] = entity.realSuperChapterId;
	data['selfVisible'] = entity.selfVisible;
	data['shareDate'] = entity.shareDate;
	data['shareUser'] = entity.shareUser;
	data['superChapterId'] = entity.superChapterId;
	data['superChapterName'] = entity.superChapterName;
	data['tags'] =  entity.tags;
	data['title'] = entity.title;
	data['type'] = entity.type;
	data['userId'] = entity.userId;
	data['visible'] = entity.visible;
	data['zan'] = entity.zan;
	return data;
}