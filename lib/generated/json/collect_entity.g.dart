import 'package:wan/generated/json/base/json_convert_content.dart';
import 'package:wan/entity/collect_entity.dart';

CollectEntity $CollectEntityFromJson(Map<String, dynamic> json) {
	final CollectEntity collectEntity = CollectEntity();
	final String? author = jsonConvert.convert<String>(json['author']);
	if (author != null) {
		collectEntity.author = author;
	}
	final int? chapterId = jsonConvert.convert<int>(json['chapterId']);
	if (chapterId != null) {
		collectEntity.chapterId = chapterId;
	}
	final String? chapterName = jsonConvert.convert<String>(json['chapterName']);
	if (chapterName != null) {
		collectEntity.chapterName = chapterName;
	}
	final int? courseId = jsonConvert.convert<int>(json['courseId']);
	if (courseId != null) {
		collectEntity.courseId = courseId;
	}
	final String? desc = jsonConvert.convert<String>(json['desc']);
	if (desc != null) {
		collectEntity.desc = desc;
	}
	final String? envelopePic = jsonConvert.convert<String>(json['envelopePic']);
	if (envelopePic != null) {
		collectEntity.envelopePic = envelopePic;
	}
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		collectEntity.id = id;
	}
	final String? link = jsonConvert.convert<String>(json['link']);
	if (link != null) {
		collectEntity.link = link;
	}
	final String? niceDate = jsonConvert.convert<String>(json['niceDate']);
	if (niceDate != null) {
		collectEntity.niceDate = niceDate;
	}
	final String? origin = jsonConvert.convert<String>(json['origin']);
	if (origin != null) {
		collectEntity.origin = origin;
	}
	final int? originId = jsonConvert.convert<int>(json['originId']);
	if (originId != null) {
		collectEntity.originId = originId;
	}
	final int? publishTime = jsonConvert.convert<int>(json['publishTime']);
	if (publishTime != null) {
		collectEntity.publishTime = publishTime;
	}
	final String? title = jsonConvert.convert<String>(json['title']);
	if (title != null) {
		collectEntity.title = title;
	}
	final int? userId = jsonConvert.convert<int>(json['userId']);
	if (userId != null) {
		collectEntity.userId = userId;
	}
	final int? visible = jsonConvert.convert<int>(json['visible']);
	if (visible != null) {
		collectEntity.visible = visible;
	}
	final int? zan = jsonConvert.convert<int>(json['zan']);
	if (zan != null) {
		collectEntity.zan = zan;
	}
	return collectEntity;
}

Map<String, dynamic> $CollectEntityToJson(CollectEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['author'] = entity.author;
	data['chapterId'] = entity.chapterId;
	data['chapterName'] = entity.chapterName;
	data['courseId'] = entity.courseId;
	data['desc'] = entity.desc;
	data['envelopePic'] = entity.envelopePic;
	data['id'] = entity.id;
	data['link'] = entity.link;
	data['niceDate'] = entity.niceDate;
	data['origin'] = entity.origin;
	data['originId'] = entity.originId;
	data['publishTime'] = entity.publishTime;
	data['title'] = entity.title;
	data['userId'] = entity.userId;
	data['visible'] = entity.visible;
	data['zan'] = entity.zan;
	return data;
}