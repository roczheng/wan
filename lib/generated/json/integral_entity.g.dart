import 'package:wan/generated/json/base/json_convert_content.dart';
import 'package:wan/entity/integral_entity.dart';

IntegralEntity $IntegralEntityFromJson(Map<String, dynamic> json) {
	final IntegralEntity integralEntity = IntegralEntity();
	final int? coinCount = jsonConvert.convert<int>(json['coinCount']);
	if (coinCount != null) {
		integralEntity.coinCount = coinCount;
	}
	final int? date = jsonConvert.convert<int>(json['date']);
	if (date != null) {
		integralEntity.date = date;
	}
	final String? desc = jsonConvert.convert<String>(json['desc']);
	if (desc != null) {
		integralEntity.desc = desc;
	}
	final int? id = jsonConvert.convert<int>(json['id']);
	if (id != null) {
		integralEntity.id = id;
	}
	final String? reason = jsonConvert.convert<String>(json['reason']);
	if (reason != null) {
		integralEntity.reason = reason;
	}
	final int? type = jsonConvert.convert<int>(json['type']);
	if (type != null) {
		integralEntity.type = type;
	}
	final int? userId = jsonConvert.convert<int>(json['userId']);
	if (userId != null) {
		integralEntity.userId = userId;
	}
	final String? userName = jsonConvert.convert<String>(json['userName']);
	if (userName != null) {
		integralEntity.userName = userName;
	}
	return integralEntity;
}

Map<String, dynamic> $IntegralEntityToJson(IntegralEntity entity) {
	final Map<String, dynamic> data = <String, dynamic>{};
	data['coinCount'] = entity.coinCount;
	data['date'] = entity.date;
	data['desc'] = entity.desc;
	data['id'] = entity.id;
	data['reason'] = entity.reason;
	data['type'] = entity.type;
	data['userId'] = entity.userId;
	data['userName'] = entity.userName;
	return data;
}